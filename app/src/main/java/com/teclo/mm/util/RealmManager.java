package com.teclo.mm.util;

import android.content.Context;

import com.teclo.mm.model.MessageMod;
import com.teclo.mm.model.request.PosicionesAct;
import com.teclo.mm.model.response.UserResponseMod;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class RealmManager {

    public final static String TAG = RealmManager.class.getSimpleName();

    private Realm realm = null;
    private Realm realmContext = null;
    private UserResponseMod userModel;
    private PosicionesAct posicionesAct;

    public RealmManager() {
        this.realm = Realm.getDefaultInstance();

        RealmResults<UserResponseMod> users = realm.where(UserResponseMod.class).findAll();
        if (users != null && users.size() > 0) {
            this.userModel = users.first();
        }
    }

    public RealmManager(Context context) {
        Realm.init(context);
        realmContext = Realm.getDefaultInstance();
}

    public Realm getRealm() {
        return realm;
    }

    /**
     * LOGIN METHODS
     */
    public UserResponseMod getUserModel() {
        return userModel;
    }

    public void setUserModel(final UserResponseMod userModel) {
        this.realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(userModel);
            }
        });
    }

    public boolean isUserLoggedIn() {
        if (userModel == null) {
            return false;
        }
        return true;
    }


    public void removeAll() {
        final RealmResults users = realm.where(UserResponseMod.class).findAll();
        //final RealmResults tokens = realm.where(TokenModel.class).findAll();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                users.deleteAllFromRealm();
                //tokens.deleteAllFromRealm();
            }
        });
    }

    /**
     * PosicionesAct
     */
    public void setPosicionesAct(final PosicionesAct posicionesAct) {
        realmContext.beginTransaction();

        // increment index
        Number currentIdNum = realmContext.where(PosicionesAct.class).max("idRealm");
        int nextId;
        if(currentIdNum == null) {
            nextId = 1;
        } else {
            nextId = currentIdNum.intValue() + 1;
        }
        posicionesAct.setIdRealm(nextId);


        realmContext.copyToRealm(posicionesAct);
        realmContext.commitTransaction();
    }

    public List<PosicionesAct> getPosicionesActs() {
        RealmResults<PosicionesAct>  posicionesActs = realmContext.where(PosicionesAct.class).findAll();

        List posicionesActList = new ArrayList<PosicionesAct>();
        for (PosicionesAct p : posicionesActs) {
            posicionesActList.add(p);
        }

        return posicionesActList;
    }

    public void deletePosicion(PosicionesAct posicionesAct) {
        RealmResults realmResults = realmContext.where(PosicionesAct.class).equalTo("idRealm", posicionesAct.getIdRealm()).findAll();
        realmContext.beginTransaction();
        realmResults.deleteAllFromRealm();
        realmContext.commitTransaction();
    }


    public void saveMessage(final MessageMod message){
        realm.beginTransaction();

        Number currentIdNum = realm.where(MessageMod.class).max("idRealm");
        int nextId;
        if(currentIdNum == null) {
            nextId = 1;
        } else {
            nextId = currentIdNum.intValue() + 1;
        }
        message.setIdRealm(nextId);

        realm.copyToRealmOrUpdate(message);
        realm.commitTransaction();
    }

    public RealmResults<MessageMod> seekMessages(){
        return realm.where(MessageMod.class).findAllSorted("date", Sort.DESCENDING);
    }

    public void deleteMessages(){
        RealmResults realmResults = realm.where(MessageMod.class).findAll();
        realm.beginTransaction();
        realmResults.deleteAllFromRealm();
        realm.commitTransaction();
    }

}
