package com.teclo.mm.util;

import android.os.AsyncTask;
import android.util.Log;

import com.teclo.mm.model.response.DeviceFirebase;
import com.teclo.mm.service.ApiEvento;
import com.teclo.mm.service.ApiToken;

import org.json.JSONObject;

import java.io.IOException;
import java.lang.ref.WeakReference;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Unitis007 on 18/10/2018.
 */

public class AsyncTaskToken extends AsyncTask<Void, Void, Response<DeviceFirebase>> {

    private static final String TAG = AsyncTaskToken.class.getName();
    public static boolean isRunning;
    private Long imei;
    private String token;

    public AsyncTaskToken(Long imei, String token){
        this.imei = imei;
        this.token = token;
    }

    @Override
    protected void onPreExecute() {
        isRunning = false;
        super.onPreExecute();
    }

    @Override
    protected Response<DeviceFirebase> doInBackground(Void... voids) {
        isRunning = true;
        ApiToken tokenService = ApiUtilsRetrofit.getTokenService();
        Call<DeviceFirebase> call = tokenService.updateToken(this.imei, this.token);

        try {
            //call the api synchronously as this is part of back ground thread already with AsncTask
            Response<DeviceFirebase> rp  =  call.execute();
            return rp;
        } catch (IOException e) {
            Log.e(TAG, "error in getting response from service using retrofit");
        }

        return null;
    }

    @Override
    protected void onPostExecute(Response<DeviceFirebase> result) {
        super.onPostExecute(result);
        isRunning = false;

        if (result != null){
            if(result.body() != null){
                DeviceFirebase respObject = result.body();
                Log.i(TAG, "Guardado");
            }else{
                //500 o 404
                String errorMessage = getMessageRequest(result);
                Log.e(TAG, errorMessage);
            }
        }else{
            //mTaskListener.onTaskCompleted(mContext.getString(R.string.non_associated_areas));
        }

    }

    public String getMessageRequest(Response response){
        try {
            JSONObject jObjError = new JSONObject(response.errorBody().string());
            return jObjError.getString("message");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
