package com.teclo.mm.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Jesus Gutierrez on 09/10/2017.
 */

public class PreferenceManager {

    private static PreferenceManager instance = null;
    private Context context;

    public static PreferenceManager sharedInstance(Context context) {
        if (instance == null) {
            instance = new PreferenceManager(context);
        }
        return instance;
    }

    private Context getContext() {
        return context;
    }

    public PreferenceManager(Context context) {
        this.context = context;
    }

    public SharedPreferences getPackagePreferences() {
        return getContext().getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
    }

}
