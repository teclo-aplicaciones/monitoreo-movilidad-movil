package com.teclo.mm.util;

import android.view.View;

import com.teclo.mm.model.MessageMod;

public interface RecyclerViewClickListener {
    void onClick(View view, MessageMod item);
}
