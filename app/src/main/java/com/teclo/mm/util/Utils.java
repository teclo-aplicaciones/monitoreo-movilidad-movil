/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.teclo.mm.util;


import android.content.Context;
import android.location.Location;
import android.text.TextUtils;

import com.teclo.mm.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Utils {

    public static String dateFormat(String format){
        Date currentDate = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.getDefault());
        String str = formatter.format(currentDate);
        return str;
    }

    public static Date toDate(String format, String value) {
        Date date = null;
        SimpleDateFormat formatter = new SimpleDateFormat(format,Locale.getDefault());
        try {
            String myDate = TextUtils.isEmpty(value) ? formatter.format(new Date()) : value;
            date = formatter.parse(myDate);
        } catch (ParseException e1) {
            e1.printStackTrace();
        }
        return date;
    }

    public static String getHourMinutes(Date date){
        DateFormat formatter = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault());
        return formatter.format(date);
    }

    public static String getDate(String format, Date value){
        SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.getDefault());
        String str = formatter.format(value);
        return str;
    }


    /**
     * Returns the {@code location} object as a human readable string.
     * @param location  The {@link Location}.
     */
    public static String getLocationText(Location location) {
        return location == null ? "Ubicación desconocida" :
                "(" + location.getLatitude() + ", " + location.getLongitude() + ")";
    }

    public static String getLocationTitle(Context context) {
        return context.getString(R.string.location_updated, DateFormat.getDateTimeInstance().format(new Date()));
    }
}
