package com.teclo.mm.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.teclo.mm.application.HttpInterceptor;
import com.teclo.mm.commons.Constants;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Jesus Gutierrez on 05/10/2017.
 */

public class ApiClientRetrofit {

    private static Retrofit retrofit = null;
    private static Retrofit retrofitGoogle = null;
    private final static String TAG = ApiClientRetrofit.class.getSimpleName();

    public static Retrofit getClient(String baseUrl) {
        if (retrofit==null) {
            Gson gson =  new GsonBuilder().setLenient().create();
            OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
            OkHttpClient client = httpClientBuilder.readTimeout(
                    Constants.CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                    .addInterceptor(new HttpInterceptor()).
                    retryOnConnectionFailure(false).build();

            retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        }
        return retrofit;
    }

    public static Retrofit getClientGoogle(String baseUrl){
        if (retrofitGoogle==null) {
            Gson gson =  new GsonBuilder().setLenient().create();
            OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
            OkHttpClient client = httpClientBuilder.readTimeout(Constants.CONNECTION_TIMEOUT, TimeUnit.SECONDS).retryOnConnectionFailure(false).build();

            retrofitGoogle = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(client)
                    .build();
        }
        return retrofitGoogle;
    }
}
