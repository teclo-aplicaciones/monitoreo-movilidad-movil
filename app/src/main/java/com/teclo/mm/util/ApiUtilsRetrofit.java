package com.teclo.mm.util;

import com.teclo.mm.commons.Constants;
import com.teclo.mm.service.ApiComandoBitacora;
import com.teclo.mm.service.ApiDirections;
import com.teclo.mm.service.ApiEvento;
import com.teclo.mm.service.ApiLocation;
import com.teclo.mm.service.ApiToken;
import com.teclo.mm.service.ApiUsers;

/**
 * Created by Jesus Gutierrez on 05/10/2017.
 */

public class ApiUtilsRetrofit {

    public static ApiUsers getUsersService(){
        return ApiClientRetrofit.getClient(Constants.BASE_URL).create(ApiUsers.class);
    }

    public static ApiLocation getLocationService(){
        return ApiClientRetrofit.getClient(Constants.BASE_URL).create(ApiLocation.class);
    }

    public static ApiDirections getDirectionService(){
        return ApiClientRetrofit.getClientGoogle(Constants.GOOGLE_DIRECTIONS).create(ApiDirections.class);
    }

    public static ApiEvento getEventoService(){
        return ApiClientRetrofit.getClient(Constants.BASE_URL).create(ApiEvento.class);
    }

    public static ApiToken getTokenService(){
        return ApiClientRetrofit.getClient(Constants.BASE_URL).create(ApiToken.class);
    }

    public static ApiComandoBitacora getComandoBitacora(){
        return ApiClientRetrofit.getClient(Constants.BASE_URL).create(ApiComandoBitacora.class);
    }
}
