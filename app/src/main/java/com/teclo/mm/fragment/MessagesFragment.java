package com.teclo.mm.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.teclo.mm.R;
import com.teclo.mm.adapter.MessagesAdapter;
import com.teclo.mm.model.MessageMod;
import com.teclo.mm.util.RealmManager;
import com.teclo.mm.util.RecyclerViewClickListener;

import java.util.List;

public class MessagesFragment extends Fragment implements View.OnClickListener {

    public final static String TAG = MessagesFragment.class.getSimpleName();

    private static String PARAM_MESSAGE = "param_message";
    public static final String BROADCAST = "mx.com.teclo.mm.broadcastmessage";

    private TextView messageNotification;
    private TextView textViewNotFound;

    private Button backButton;
    private RecyclerView recyclerViewMessage;
    private MessagesAdapter mMessagesAdapter;

    private ReceiverMessage mReceiver;

    private ActionsFromMessage mCallBack;
    private RealmManager realmManager;

    public MessagesFragment() {
        // Required empty public constructor
    }

    public static MessagesFragment newInstance(String message) {
        MessagesFragment fragment = new MessagesFragment();
        Bundle args = new Bundle();
        args.putString(PARAM_MESSAGE, message);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_messages, container, false);
        textViewNotFound = rootView.findViewById(R.id.not_found_messages);


        //messageNotification = rootView.findViewById(R.id.messageNotification);
        //backButton = rootView.findViewById(R.id.backButton);

        Bundle args = getArguments();
        String messageArgument = args.getString(PARAM_MESSAGE, "");

        //messageNotification.setText(messageArgument);
        //backButton.setOnClickListener(this);

        recyclerViewMessage = rootView.findViewById(R.id.recycler_message);
        recyclerViewMessage.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewMessage.setHasFixedSize(true);

        RecyclerViewClickListener listener = new RecyclerViewClickListener() {
            @Override
            public void onClick(View view, MessageMod item) {
                mCallBack.goDetailMessage(item);
            }
        };
        List<MessageMod> listMessages = realmManager.seekMessages();

        if(listMessages.size() < 1){
            textViewNotFound.setVisibility(View.VISIBLE);
        }

        mMessagesAdapter = new MessagesAdapter(listMessages, listener);
        recyclerViewMessage.setAdapter(mMessagesAdapter);

        mReceiver = new ReceiverMessage();
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.action_remove_messages:
                realmManager.deleteMessages();
                mMessagesAdapter.notifyDataSetChanged();
                textViewNotFound.setVisibility(View.VISIBLE);
                return true;
        }
        return false;
    }


    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mReceiver, new IntentFilter(BROADCAST));
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mReceiver);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        realmManager = new RealmManager();
        try{
            mCallBack = (ActionsFromMessage) context;
        }catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement ActionsFromMessage");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        int component = v.getId();
        switch (component) {
            /*case R.id.backButton :
                mCallBack.goBack();
                break;*/
        }

    }

    /**
     * Receiver for broadcasts sent by {@link com.teclo.mm.service.FirebaseMessagingService}.
     */
    private class ReceiverMessage extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            mMessagesAdapter.notifyDataSetChanged();

            if(mMessagesAdapter.getItemCount() > 0){
                textViewNotFound.setVisibility(View.GONE);
            }

            //List<MessageMod> listMessages = realmManager.seekMessages();
            //mMessagesAdapter = new MessagesAdapter(listMessages);
            //messageNotification.setText(intent.getStringExtra("body"));
        }
    }

    public interface ActionsFromMessage{
        void goBack();
        void goDetailMessage(MessageMod message);
    }
}
