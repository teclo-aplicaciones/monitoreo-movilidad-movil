package com.teclo.mm.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.teclo.mm.R;
import com.teclo.mm.activity.MainActivity;
import com.teclo.mm.adapter.PlaceAutocompleteAdapter;
import com.teclo.mm.commons.Constants;
import com.teclo.mm.model.Google.GoogleLegMod;
import com.teclo.mm.model.Google.GoogleResponse;
import com.teclo.mm.model.Google.GoogleRouteMod;
import com.teclo.mm.model.request.RouteMod;
import com.teclo.mm.model.response.RouteResponseMod;
import com.teclo.mm.service.LocationUpdatesService;
import com.teclo.mm.util.ApiUtilsRetrofit;
import com.teclo.mm.util.PreferenceManager;
import com.teclo.mm.util.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsFragment extends Fragment implements OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, LocationListener {

    public final static String TAG = MapsFragment.class.getSimpleName();

    protected GoogleApiClient mGoogleApiClient;
    private GoogleMap mMap;

    private static String[] options = null;
    private String optionSelected = "driving";
    private String mIdOrigen, mIdDestino;
    private GoogleRouteMod mRouteGenerated;

    //Location
    private Location mLastLocation;
    private LocationUpdatesService mService = null;
    // Tracks the bound state of the service.
    private boolean mBound = false;
    // The BroadcastReceiver used to listen from broadcasts from the service.
    private MyReceiver myReceiver;

    //Adapter
    private PlaceAutocompleteAdapter mAdapter;

    //Parent Activity
    private MainActivity mActivity;

    //UI Elements
    private View mapView;
    private Button mBuscar;
    private Button mIniciar;
    private Button mTerminar;
    private TextView mDistance;
    private TextView mDuration;
    private ImageView mTypeRide;
    private AlertDialog alertDialogRide;
    private ProgressDialog progressDialog;
    private AutoCompleteTextView mAutocompleteOrigen;
    private AutoCompleteTextView mAutocompleteDestino;

    //Map Objects
    private List<Marker> originMarkers = new ArrayList<>();
    private List<Marker> destinationMarkers = new ArrayList<>();
    private List<Polyline> polylinePaths = new ArrayList<>();

    public MapsFragment() {
    }

    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LocationUpdatesService.LocalBinder binder = (LocationUpdatesService.LocalBinder) service;
            mService = binder.getService();

            //SharedPreferences sharedPreferences = PreferenceManager.sharedInstance(mActivity).getPackagePreferences();
            //if (sharedPreferences.getBoolean(Constants.KEY_REQUESTING_LOCATION_UPDATES, false)) {
                mService.requestLocationUpdates();
            //}

            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        connectGoogleApiClient();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(myReceiver, new IntentFilter(LocationUpdatesService.ACTION_BROADCAST));
    }

    @Override
    public void onPause() {
        super.onPause();
        disconnectGoogleApiClient();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(myReceiver);
    }

    @Override
    public void onStop() {
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            getActivity().unbindService(mServiceConnection);
            mBound = false;
        }
        super.onStop();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_maps, container, false);

        //GET Views
        options = getActivity().getResources().getStringArray(R.array.option_radio_labels);

        mBuscar = (Button) rootView.findViewById(R.id.find_button);
        mIniciar = (Button) rootView.findViewById(R.id.star_button);
        mTerminar = (Button) rootView.findViewById(R.id.finish_button);

        mTypeRide = (ImageView) rootView.findViewById(R.id.imgRide);
        mAutocompleteOrigen = (AutoCompleteTextView) rootView.findViewById(R.id.origin);
        mAutocompleteDestino = (AutoCompleteTextView) rootView.findViewById(R.id.destination);
        mDistance = (TextView) rootView.findViewById(R.id.tvDistance);
        mDuration = (TextView) rootView.findViewById(R.id.tvDuration);

        SharedPreferences sharedPreferences = PreferenceManager.sharedInstance(mActivity).getPackagePreferences();
        if (sharedPreferences.getBoolean(Constants.KEY_REQUESTING_LOCATION_UPDATES, false)) {
            mActivity.bindService(new Intent(mActivity, LocationUpdatesService.class), mServiceConnection, Context.BIND_AUTO_CREATE);
        }

        //Initialize Objects
        mAdapter = new PlaceAutocompleteAdapter(mActivity, mGoogleApiClient, Constants.BOUNDS_MEXICO);
        mAutocompleteOrigen.setAdapter(mAdapter);
        mAutocompleteDestino.setAdapter(mAdapter);

        //SET Events
        mBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSelectRide();
            }
        });
        mIniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmarIniciarViaje();
            }
        });
        mTerminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mService.removeLocationUpdates();

                getActivity().unbindService(mServiceConnection);
                mBound = false;

                mBuscar.setVisibility(View.VISIBLE);
                mIniciar.setVisibility(View.GONE);
                mTerminar.setVisibility(View.GONE);

                mDistance.setText(R.string.distance_description);
                mDuration.setText(R.string.time_description);

                mAutocompleteOrigen.setText("");
                mAutocompleteDestino.setText("");

                cleanMap();
            }
        });

        mAutocompleteOrigen.setOnItemClickListener(mAutocompleteClickListener);
        mAutocompleteDestino.setOnItemClickListener(mAutocompleteClickListener);

        myReceiver = new MyReceiver();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapView = mapFragment.getView();
        mapFragment.getMapAsync(this);

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
            mActivity = (MainActivity) context;
        }
        //Create Google API Client
        buildGoogleApiClient();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        disconnectGoogleApiClient();
    }

    @Override
    public void onDestroy() {
        if (mBound) {
            getActivity().unbindService(mServiceConnection);
            mBound = false;
        }
        disconnectGoogleApiClient();
        super.onDestroy();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(false);

        if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
            mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                @Override
                public boolean onMyLocationButtonClick() {
                    getAddress();
                    return false;
                }
            });
        }


        if (mapView != null &&
                mapView.findViewById(Integer.parseInt("1")) != null) {
            // Get the button view
            View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            // and next place it, on bottom right (as Google Maps app)
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                    locationButton.getLayoutParams();
            // position on right bottom
            layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            layoutParams.setMargins(0, 0, 30, 30);
        }


        SharedPreferences sharedPreferences = PreferenceManager.sharedInstance(mActivity).getPackagePreferences();
        if (sharedPreferences.getBoolean(Constants.KEY_REQUESTING_LOCATION_UPDATES, false)) {
            mBuscar.setVisibility(View.GONE);
            mIniciar.setVisibility(View.GONE);
            mTerminar.setVisibility(View.VISIBLE);

            GoogleRouteMod ruta_generada = new com.google.gson.Gson().fromJson(sharedPreferences.getString(Constants.KEY_ROUTE_GENERATED, ""), GoogleRouteMod.class);
            drawInMap(ruta_generada, true);

        }
    }

    private void dialogSelectRide() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle(R.string.title_alert_ride);
        builder.setSingleChoiceItems(options, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                switch (item) {
                    case 0:
                        //Auto
                        mTypeRide.setImageResource(R.drawable.ic_car);
                        optionSelected = "driving";
                        break;
                    case 1:
                        //Bicicleta
                        mTypeRide.setImageResource(R.drawable.ic_bike);
                        optionSelected = "bicycling";
                        break;
                    case 2:
                        //A pie
                        mTypeRide.setImageResource(R.drawable.ic_walk);
                        optionSelected = "walking";
                        break;
                }
                alertDialogRide.dismiss();
                buscarRuta();
            }
        });
        alertDialogRide = builder.create();
        alertDialogRide.show();
    }

    private void getLastLocation() {
        if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }
    }

    private void getAddress() {
        if (mLastLocation != null) {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(mActivity, Locale.getDefault());
            try {
                String sPlace;
                addresses = geocoder.getFromLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude(), 1);
                String address = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getAddressLine(1);
                String country = addresses.get(0).getAddressLine(2);

                String[] splitAddress = address.split(",");
                sPlace = splitAddress[0] + "\n";
                if (city != null && !city.isEmpty()) {
                    String[] splitCity = city.split(",");
                    sPlace += splitCity[0];
                }

            } catch (IOException e) {
                mAutocompleteOrigen.setText("");
                e.printStackTrace();
            }
        }
    }

    protected synchronized void buildGoogleApiClient() {
        // Construct a GoogleApiClient for the {@link Places#GEO_DATA_API} using AutoManage
        // functionality, which automatically sets up the API client to handle Activity lifecycle
        // events. If your activity does not extend FragmentActivity, make sure to call connect()
        // and disconnect() explicitly.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(mActivity)
                    //.enableAutoManage(this, 0 /* clientId */, this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    private void disconnectGoogleApiClient() {
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private void connectGoogleApiClient() {
        if (!mGoogleApiClient.isConnected() && !mGoogleApiClient.isConnecting()) {
            mGoogleApiClient.connect();
        }
    }

    private void buscarRuta() {
        String origin = mAutocompleteOrigen.getText().toString();
        String destination = mAutocompleteDestino.getText().toString();

        if (origin.isEmpty()) {
            Toast.makeText(mActivity, R.string.required_origin, Toast.LENGTH_SHORT).show();
            return;
        }
        if (destination.isEmpty()) {
            Toast.makeText(mActivity, R.string.required_destination, Toast.LENGTH_SHORT).show();
            return;
        }

        progressDialog = new ProgressDialog(mActivity, ProgressDialog.STYLE_SPINNER); //R.style.AppTheme_Dark_Dialog
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.search_route_dialog_message));
        progressDialog.show();

        cleanMap();

        String origen = "place_id:" + mIdOrigen;
        String destino = "place_id:" + mIdDestino;
        ApiUtilsRetrofit.getDirectionService().getGoogleDirection(origen, destino, optionSelected, "es", getString(R.string.google_key_api)).enqueue(new Callback<GoogleResponse>() {
            @Override
            public void onResponse(Call<GoogleResponse> call, Response<GoogleResponse> response) {
                if (response.isSuccessful()){
                    GoogleResponse result = response.body();
                    drawInMap(result.getRoutes().get(0), false);

                    mBuscar.setVisibility(View.GONE);
                    mIniciar.setVisibility(View.VISIBLE);

                }else{
                    Toast.makeText(mActivity, getString(R.string.not_found_routes), Toast.LENGTH_SHORT).show();
                }

                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<GoogleResponse> call, Throwable t) {
                Log.d(TAG, t.toString());
                progressDialog.dismiss();
            }
        });
        //new DirectionFinder(this, mActivity, mIdOrigen, mIdDestino, optionSelected).execute();
    }

    private void confirmarIniciarViaje() {
        new AlertDialog.Builder(mActivity, R.style.AlertDialogCustom)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getString(R.string.title_start_route))
                .setMessage(getString(R.string.confirm_start_route))
                .setPositiveButton(getString(R.string.generic_positive_answer), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        iniciarViaje();
                    }

                })
                .setNegativeButton(getString(R.string.generic_negative_answer), null)
                .setNeutralButton(getString(R.string.action_search_answer), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        cleanMap();

                        mAutocompleteOrigen.setText("");
                        mAutocompleteDestino.setText("");

                        mBuscar.setVisibility(View.VISIBLE);
                        mIniciar.setVisibility(View.GONE);

                        mIdOrigen = "";
                        mIdDestino = "";
                    }
                })
                .show();
    }

    private void iniciarViaje() {
        progressDialog = new ProgressDialog(mActivity, ProgressDialog.STYLE_SPINNER); //R.style.AppTheme_Dark_Dialog
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.start_route_dialog_message));
        progressDialog.show();


        GoogleLegMod leg = mRouteGenerated.getLegs().get(0);
        RouteMod routeObj = new RouteMod(leg.getStartLocation().getLatitud(), leg.getStartLocation().getLongitud(),
                leg.getStartAddress(), leg.getEndLocation().getLatitud(), leg.getEndLocation().getLongitud(),
                leg.getEndAddress(),  Utils.dateFormat("yyyy/MM/dd HH:mm:ss"), optionSelected);


        ApiUtilsRetrofit.getLocationService().postRuta(routeObj).enqueue(new Callback<RouteResponseMod>() {
            @Override
            public void onResponse(Call<RouteResponseMod> call, Response<RouteResponseMod> response) {
                if (response.isSuccessful()){
                    RouteResponseMod result = response.body();

                    SharedPreferences.Editor edit = PreferenceManager.sharedInstance(mActivity).getPackagePreferences().edit();
                    edit.putLong(Constants.KEY_ROUTE, result.getIdRuta());
                    edit.putString(Constants.KEY_ROUTE_GENERATED, mRouteGenerated.toString());
                    edit.commit();

                    mActivity.bindService(new Intent(mActivity, LocationUpdatesService.class), mServiceConnection, Context.BIND_AUTO_CREATE);
                    //mService.requestLocationUpdates();

                    mIniciar.setVisibility(View.GONE);
                    mTerminar.setVisibility(View.VISIBLE);
                }else{
                    Toast.makeText(mActivity, response.message(), Toast.LENGTH_SHORT).show();
                }

                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<RouteResponseMod> call, Throwable t) {
                Log.d(TAG, t.toString());
                progressDialog.dismiss();
                Toast.makeText(mActivity, getString(R.string.server_error) ,Toast.LENGTH_SHORT).show();
            }
        });

        /*HttpRequest.saveFoundRouteRequest(mActivity, mRouteGenerated, new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {

            }

            @Override
            public void onSuccessResponse(JSONObject result) throws JSONException {
                Long ruta_id = result.getLong("idRuta");
                Utils.savePreferences(mActivity, Constants.KEY_ROUTE, ruta_id);
                Utils.savePreferences(mActivity, Constants.KEY_ROUTE_GENERATED, mRouteGenerated.toString());

                mService.requestLocationUpdates();

                mIniciar.setVisibility(View.GONE);
                mTerminar.setVisibility(View.VISIBLE);

                progressDialog.dismiss();
            }

            @Override
            public void onErrorResponse(String message) {
                progressDialog.dismiss();
                Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show();
            }
        });*/
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getLastLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        getLastLocation();
    }

    private AdapterView.OnItemClickListener mAutocompleteClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*
             Retrieve the place ID of the selected item from the Adapter.
             The adapter stores each Place suggestion in a AutocompletePrediction from which we
             read the place ID and title.
              */
            final AutocompletePrediction item = mAdapter.getItem(position);
            final String placeId = item.getPlaceId();
            final CharSequence primaryText = item.getPrimaryText(null);

            /*
             Issue a request to the Places Geo Data API to retrieve a Place object with additional
             details about the place.
              */
            //PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId);
            View whichAutocomplete = getActivity().getCurrentFocus();
            if (whichAutocomplete instanceof AutoCompleteTextView) {
                AutoCompleteTextView control = (AutoCompleteTextView) whichAutocomplete;
                if (control.getId() == R.id.origin) {
                    mIdOrigen = placeId;
                } else if (control.getId() == R.id.destination) {
                    mIdDestino = placeId;
                }
                control.setText(primaryText);
            }

            //placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
            //Log.i(TAG, "Called getPlaceById to get Place details for " + placeId);
        }
    };


    private void drawInMap(GoogleRouteMod route, Boolean fromResume){
        if(!fromResume){
            //route.setModeRide(optionSelected);
        }else{
            //changeImage(route.getModeRide());
        }

        mRouteGenerated = route;
        mDuration.setText(route.getLegs().get(0).getDuration().getText());
        mDistance.setText(route.getLegs().get(0).getDistance().getText());

        LatLng startLocation = new LatLng(route.getLegs().get(0).getStartLocation().getLatitud(),
                route.getLegs().get(0).getStartLocation().getLongitud());

        LatLng endLocation = new LatLng(route.getLegs().get(0).getEndLocation().getLatitud(),
                route.getLegs().get(0).getEndLocation().getLongitud());

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(startLocation, 16));

        originMarkers.add(mMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                .title(route.getLegs().get(0).getStartAddress())
                .position(startLocation)));

        destinationMarkers.add(mMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                .title(route.getLegs().get(0).getEndAddress())
                .position(endLocation)));

        PolylineOptions polylineOptions = new PolylineOptions().
                geodesic(true).
                color(Color.BLUE).
                width(5);

        List<LatLng> points = decodePolyLine(route.getOverviewPolyline().getPoints());

        for (int i = 0; i < points.size(); i++)
            polylineOptions.add(points.get(i));

        polylinePaths.add(mMap.addPolyline(polylineOptions));
    }

    private void cleanMap(){
        if (originMarkers != null) {
            for (Marker marker : originMarkers) {
                marker.remove();
            }
        }

        if (destinationMarkers != null) {
            for (Marker marker : destinationMarkers) {
                marker.remove();
            }
        }

        if (polylinePaths != null) {
            for (Polyline polyline:polylinePaths ) {
                polyline.remove();
            }
        }
    }

    private void changeImage(String ride){
        if(ride.equals("driving")){
            mTypeRide.setImageResource(R.drawable.ic_car);
        }else if(ride.equals("bicycling")){
            mTypeRide.setImageResource(R.drawable.ic_bike);
        }else if(ride.equals("walking")){
            mTypeRide.setImageResource(R.drawable.ic_walk);
        }
    }

    private List<LatLng> decodePolyLine(final String poly) {
        int len = poly.length();
        int index = 0;
        List<LatLng> decoded = new ArrayList<LatLng>();
        int lat = 0;
        int lng = 0;

        while (index < len) {
            int b;
            int shift = 0;
            int result = 0;
            do {
                b = poly.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = poly.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            decoded.add(new LatLng(
                    lat / 100000d, lng / 100000d
            ));
        }

        return decoded;
    }

    /**
     * Receiver for broadcasts sent by {@link LocationUpdatesService}.
     */
    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra(LocationUpdatesService.EXTRA_LOCATION);
            if (location != null) {
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(/*current latitude*/location.getLatitude(), /*current longitude*/location.getLongitude()))      // Sets the center of the map to location user
                        .zoom(16)                   // Sets the zoom
                        .build();                   // Creates a CameraPosition from the builder
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                Toast.makeText(mActivity, Utils.getLocationText(location), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
