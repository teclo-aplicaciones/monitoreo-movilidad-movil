package com.teclo.mm.fragment;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.teclo.mm.R;
import com.teclo.mm.activity.MainActivity;
import com.teclo.mm.service.LocationUpdatesService;
import com.teclo.mm.commons.Constants;
import com.teclo.mm.util.PreferenceManager;
import com.teclo.mm.util.Utils;

public class CoordinatesFragment extends Fragment {

    public static String TAG = CoordinatesFragment.class.getSimpleName();

    //UI Elements
    TextInputLayout mMinutosInputLayout;
    EditText mMinutos;
    Button mIniciar;
    Button mDetener;

    // The BroadcastReceiver used to listen from broadcasts from the service.
    private MyReceiver myReceiver;

    // A reference to the service used to get location updates.
    private LocationUpdatesService mService = null;

    // Tracks the bound state of the service.
    private boolean mBound = false;

    //Parent Activity
    private MainActivity mActivity;

    public CoordinatesFragment() {
        // Required empty public constructor
    }

    // Monitors the state of the connection to the service.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LocationUpdatesService.LocalBinder binder = (LocationUpdatesService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_coordinates, container, false);

        mIniciar = rootView.findViewById(R.id.transmitir_iniciar);
        mDetener = rootView.findViewById(R.id.transmitir_detener);

        //mMinutosInputLayout = (TextInputLayout) rootView.findViewById(R.id.minutes_user);
        //mMinutos = (EditText) rootView.findViewById(R.id.minutes);
        mIniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startCoordinates();
            }
        });

        mDetener.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopCoordinates();
            }
        });

        mActivity.bindService(new Intent(mActivity, LocationUpdatesService.class), mServiceConnection, Context.BIND_AUTO_CREATE);
        myReceiver = new MyReceiver();
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
            mActivity = (MainActivity) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(myReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshUI();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(myReceiver, new IntentFilter(LocationUpdatesService.ACTION_BROADCAST));
    }

    @Override
    public void onStop() {
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            getActivity().unbindService(mServiceConnection);
            mBound = false;
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            getActivity().unbindService(mServiceConnection);
            mBound = false;
        }
        super.onDestroy();
    }

    private void startCoordinates(){
        mService.requestLocationUpdates();
        refreshUI();
/*
        String minutos = mMinutos.getText().toString();
        if(TextUtils.isEmpty(minutos)){
            toggleTextInputLayoutError(mMinutosInputLayout, getString(R.string.action_1_error_field));
        }else{
            toggleTextInputLayoutError(mMinutosInputLayout, null);
            milisecondsRequestUpdate();
            mService.requestLocationUpdates();
            refreshUI();
        }
*/
    }

    private void stopCoordinates(){
        SharedPreferences sharedPreferences = PreferenceManager.sharedInstance(getActivity()).getPackagePreferences();
        Long minutes = sharedPreferences.getLong(Constants.KEY_INTERVAL_LOCATION_UPDATE, 60000L) / 60000L;
        //mMinutos.setText(String.valueOf(minutes));
        mService.removeLocationUpdates();

        refreshUI();
    }

    private void milisecondsRequestUpdate(){
        SharedPreferences.Editor edit = PreferenceManager.sharedInstance(getActivity()).getPackagePreferences().edit();
        Long minutes = Long.valueOf(mMinutos.getText().toString());
        Long miliseconds = minutes * 60000L;
        edit.putLong(Constants.KEY_INTERVAL_LOCATION_UPDATE, miliseconds);
        edit.commit();
    }

    private void toggleTextInputLayoutError(@NonNull TextInputLayout textInputLayout, String msg) {
        if (msg == null) {
            textInputLayout.setErrorEnabled(false);
        } else {
            textInputLayout.setErrorEnabled(true);
        }
        textInputLayout.setError(msg);
    }

    private void refreshUI(){
        SharedPreferences sharedPreferences = PreferenceManager.sharedInstance(getActivity()).getPackagePreferences();

        if(sharedPreferences.getBoolean(Constants.KEY_REQUESTING_LOCATION_UPDATES, false)){
            mDetener.setEnabled(true);
            mIniciar.setEnabled(false);
            //mMinutos.setText(String.valueOf(Utils.getLongPreference(getActivity(), Constants.KEY_INTERVAL_LOCATION_UPDATE) / 60000L));
        }else{
            mDetener.setEnabled(false);
            mIniciar.setEnabled(true);
            //mMinutos.setText("");
        }
    }

    /**
     * Receiver for broadcasts sent by {@link LocationUpdatesService}.
     */
    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra(LocationUpdatesService.EXTRA_LOCATION);
            if (location != null) {
                Toast.makeText(mActivity, Utils.getLocationText(location), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
