package com.teclo.mm.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.teclo.mm.R;
import com.teclo.mm.util.Utils;

import java.util.Date;

import co.intentservice.chatui.ChatView;
import co.intentservice.chatui.models.ChatMessage;

public class FriendsFragment extends Fragment {

    public final static String TAG = FriendsFragment.class.getSimpleName();

    private static String PARAM_MESSAGE = "param_message";
    private static String PARAM_DATE = "param_date";

    private ActionsFromFriends mCallBack;

    public FriendsFragment() {
    }

    public static FriendsFragment newInstance(String message, String date) {
        FriendsFragment fragment = new FriendsFragment();
        Bundle args = new Bundle();
        args.putString(PARAM_MESSAGE, message);
        args.putString(PARAM_DATE, date);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_friends, container, false);
        ChatView chatView = rootView.findViewById(R.id.chat_view);

        Bundle args = getArguments();
        String messageArgument = args.getString(PARAM_MESSAGE, "");
        String dateArgument = args.getString(PARAM_DATE, "");

        chatView.addMessage(new ChatMessage(messageArgument, millis(dateArgument), ChatMessage.Type.RECEIVED));

        chatView.setOnSentMessageListener(new ChatView.OnSentMessageListener() {
            @Override
            public boolean sendMessage(ChatMessage chatMessage) { return true; }
        });

        chatView.setTypingListener(new ChatView.TypingListener() {
            @Override
            public void userStartedTyping() { }

            @Override
            public void userStoppedTyping() { }
        });


        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        /*try{
            mCallBack = (FriendsFragment.ActionsFromFriends) context;
        }catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement ActionsFromFriends");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //setHasOptionsMenu(true);
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            mCallBack.goBackToMessages();
            return true;
        }
        return false;
    }*/

    private long millis(String date){
        Date dateConvert = Utils.toDate("dd/MM/yyyy HH:mm:ss", date);
        if(date != null){
            return dateConvert.getTime();
        }

        return System.currentTimeMillis();
    }

    public interface ActionsFromFriends{
        void goBackToMessages();
    }
}
