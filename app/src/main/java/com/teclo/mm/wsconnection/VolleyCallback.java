package com.teclo.mm.wsconnection;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Jesus Gutierrez on 18/08/2017.
 */

public interface VolleyCallback {
    void onSuccessResponse(String result);
    void onSuccessResponse(JSONObject result) throws JSONException;
    void onErrorResponse(String message);
}
