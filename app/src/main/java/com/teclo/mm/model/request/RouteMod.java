package com.teclo.mm.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Jesus Gutierrez on 18/10/2017.
 */

public class RouteMod {

    @SerializedName("origenLatitud")
    private double origenLatitud;

    @SerializedName("origenLongitud")
    private double origenLongitud;

    @SerializedName("origenDescripcion")
    private String origenDescripcion;

    @SerializedName("destinoLatitud")
    private double destinoLatitud;

    @SerializedName("destinoLongitud")
    private double destinoLongitud;

    @SerializedName("destinoDescripcion")
    private String destinoDescripcion;

    @SerializedName("fechaCreacionRuta")
    private String fecha;

    @SerializedName("cdTipoRecorrido")
    private String modo;

    public RouteMod(double origenLatitud, double origenLongitud, String origenDescripcion,
                    double destinoLatitud, double destinoLongitud, String destinoDescripcion,
                    String fecha, String modo){

        this.origenLatitud = origenLatitud;
        this.origenLongitud = origenLongitud;
        this.origenDescripcion = origenDescripcion;

        this.destinoLatitud = destinoLatitud;
        this.destinoLongitud = destinoLongitud;
        this.destinoDescripcion = destinoDescripcion;

        this.fecha = fecha;
        this.modo = modo;
    }

}
