package com.teclo.mm.model.request;

public class UserMod {

    private String username;
    private String password;

    public UserMod(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
