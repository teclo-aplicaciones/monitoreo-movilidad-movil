package com.teclo.mm.model.Google;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jesus Gutierrez on 16/10/2017.
 */

public class GoogleLegMod {

    @SerializedName("distance")
    private GoogleDistance distance;

    @SerializedName("duration")
    private GoogleDuration duration;

    @SerializedName("end_address")
    private String endAddress;

    @SerializedName("start_address")
    @Expose
    private String startAddress;

    @SerializedName("end_location")
    @Expose
    private GoogleEndLocation endLocation;

    @SerializedName("start_location")
    @Expose
    private GoogleStartLocation startLocation;

    public GoogleDistance getDistance() {
        return distance;
    }

    public void setDistance(GoogleDistance distance) {
        this.distance = distance;
    }

    public GoogleDuration getDuration() {
        return duration;
    }

    public void setDuration(GoogleDuration duration) {
        this.duration = duration;
    }

    public String getEndAddress() {
        return endAddress;
    }

    public void setEndAddress(String endAddress) {
        this.endAddress = endAddress;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public GoogleEndLocation getEndLocation() {
        return endLocation;
    }

    public void setEndLocation(GoogleEndLocation endLocation) {
        this.endLocation = endLocation;
    }

    public GoogleStartLocation getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(GoogleStartLocation startLocation) {
        this.startLocation = startLocation;
    }
}
