package com.teclo.mm.model.Google;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Jesus Gutierrez on 17/10/2017.
 */

public class GoogleResponse {

    @SerializedName("status")
    private String status;

    @SerializedName("routes")
    private List<GoogleRouteMod> routes = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<GoogleRouteMod> getRoutes() {
        return routes;
    }

    public void setRoutes(List<GoogleRouteMod> routes) {
        this.routes = routes;
    }
}
