package com.teclo.mm.model.request;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class PosicionesAct extends RealmObject {

    @PrimaryKey
    private int idRealm;

    @SerializedName("idPosicionesAct")
    private Long idPosicionesAct;

    @SerializedName("idDispositivo")
    private Long idDispositivo;

    @SerializedName("nuLatitud")
    private double nuLatitud;

    @SerializedName("nuLongitud")
    private double nuLongitud;

    @SerializedName("nuAltitud")
    private double nuAltitud;

    @SerializedName("nuAccuracy")
    private double nuAccuracy;

    @SerializedName("nuImei")
    private Long nuImei;

    @SerializedName("nuIp")
    private String nuIp;

    @SerializedName("fhRegistro")
    private String fhRegistro;

    @SerializedName("idEvento")
    private Long idEvento;


    public double getNuAccuracy() {
        return nuAccuracy;
    }

    public void setNuAccuracy(double nuAccuracy) {
        this.nuAccuracy = nuAccuracy;
    }

    public Long getIdPosicionesAct() {
        return idPosicionesAct;
    }

    public void setIdPosicionesAct(Long idPosicionesAct) {
        this.idPosicionesAct = idPosicionesAct;
    }

    public Long getIdDispositivo() {
        return idDispositivo;
    }

    public void setIdDispositivo(Long idDispositivo) {
        this.idDispositivo = idDispositivo;
    }

    public double getNuLatitud() {
        return nuLatitud;
    }

    public void setNuLatitud(double nuLatitud) {
        this.nuLatitud = nuLatitud;
    }

    public double getNuLongitud() {
        return nuLongitud;
    }

    public void setNuLongitud(double nuLongitud) {
        this.nuLongitud = nuLongitud;
    }

    public double getNuAltitud() {
        return nuAltitud;
    }

    public void setNuAltitud(double nuAltitud) {
        this.nuAltitud = nuAltitud;
    }

    public Long getNuImei() {
        return nuImei;
    }

    public void setNuImei(Long nuImei) {
        this.nuImei = nuImei;
    }

    public String getNuIp() {
        return nuIp;
    }

    public void setNuIp(String nuIp) {
        this.nuIp = nuIp;
    }

    public String getFhRegistro() {
        return fhRegistro;
    }

    public void setFhRegistro(String fhRegistro) {
        this.fhRegistro = fhRegistro;
    }

    public Long getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(Long idEvento) {
        this.idEvento = idEvento;
    }

    public int getIdRealm() {
        return idRealm;
    }

    public void setIdRealm(int idRealm) {
        this.idRealm = idRealm;
    }

    @Override
    public String toString() {
        return "PosicionesAct{" +
                "idPosicionesAct=" + idPosicionesAct +
                ", idDispositivo=" + idDispositivo +
                ", nuLatitud=" + nuLatitud +
                ", nuLongitud=" + nuLongitud +
                ", nuAltitud=" + nuAltitud +
                ", nuAccuracy=" + nuAccuracy +
                ", nuImei=" + nuImei +
                ", nuIp='" + nuIp + '\'' +
                ", fhRegistro='" + fhRegistro + '\'' +
                ", idEvento=" + idEvento +
                '}';
    }
}
