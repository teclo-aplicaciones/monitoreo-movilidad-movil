package com.teclo.mm.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Jesus Gutierrez on 18/10/2017.
 */

public class RouteResponseMod {

    @SerializedName("idRuta")
    private Long idRuta;

    public Long getIdRuta() {
        return idRuta;
    }

    public void setIdRuta(Long idRuta) {
        this.idRuta = idRuta;
    }
}
