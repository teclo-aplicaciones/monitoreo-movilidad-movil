package com.teclo.mm.model.response;

import com.google.gson.annotations.SerializedName;
import com.teclo.mm.model.request.PosicionesAct;

import io.realm.RealmObject;

public class PosicionesActResponse{
    @SerializedName("status")
    private Status status;
    @SerializedName("data")
    private PosicionesAct data;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public PosicionesAct getData() {
        return data;
    }

    public void setData(PosicionesAct data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RequestPosicionesActWS{" +
                "status=" + status +
                ", data=" + data +
                '}';
    }


}
