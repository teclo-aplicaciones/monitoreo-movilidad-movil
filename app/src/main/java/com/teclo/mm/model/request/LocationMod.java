package com.teclo.mm.model.request;


import com.google.gson.annotations.SerializedName;

public class LocationMod {

    @SerializedName("employeeId")
    private Long employeeId;

    @SerializedName("eventTypeCode")
    private String eventTypeCode;

    @SerializedName("articleId")
    private Long articleId;

    @SerializedName("latitude")
    private double latitude;

    @SerializedName("longitude")
    private double longitude;

    @SerializedName("serialNumber")
    private String serialNumber;

    @SerializedName("infringement")
    private String infringement;

    //Format (yyyy/MM/dd HH:mm:ss)
    @SerializedName("eventDate")
    private String eventDate;


    public LocationMod(Long employeeId, String eventTypeCode, Long articleId, double latitude, double longitude, String serialNumber, String infringement, String eventDate) {
        this.employeeId = employeeId;
        this.eventTypeCode = eventTypeCode;
        this.articleId = articleId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.serialNumber = serialNumber;
        this.infringement = infringement;
        this.eventDate = eventDate;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getEventTypeCode() {
        return eventTypeCode;
    }

    public void setEventTypeCode(String eventTypeCode) {
        this.eventTypeCode = eventTypeCode;
    }


    public Long getArticleId() {
        return articleId;
    }

    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getInfringement() {
        return infringement;
    }

    public void setInfringement(String infringement) {
        this.infringement = infringement;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    @Override
    public String toString() {
        return "LocationMod{" +
                "employeeId=" + employeeId +
                ", eventTypeCode='" + eventTypeCode + '\'' +
                ", articleId=" + articleId +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", serialNumber='" + serialNumber + '\'' +
                ", infringement='" + infringement + '\'' +
                ", eventDate='" + eventDate + '\'' +
                '}';
    }
}
