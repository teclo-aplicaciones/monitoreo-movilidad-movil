package com.teclo.mm.model.Google;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jesus Gutierrez on 17/10/2017.
 */

public class GoogleStartLocation {

    @SerializedName("lat")
    @Expose
    private double latitud;

    @SerializedName("lng")
    @Expose
    private double longitud;

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double lat) {
        this.latitud = lat;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double lng) {
        this.longitud = lng;
    }
}
