package com.teclo.mm.model.Google;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Jesus Gutierrez on 17/10/2017.
 */

public class GoogleDuration {

    @SerializedName("text")
    private String text;

    @SerializedName("value")
    private Integer value;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
