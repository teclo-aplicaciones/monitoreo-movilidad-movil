package com.teclo.mm.model.Google;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Jesus Gutierrez on 16/10/2017.
 */

public class GoogleRouteMod {

    @SerializedName("legs")
    @Expose
    private List<GoogleLegMod> legs = null;

    @Expose
    @SerializedName("overview_polyline")
    private GoogleOverviewPolyline overviewPolyline;

    public List<GoogleLegMod> getLegs() {
        return legs;
    }

    public void setLegs(List<GoogleLegMod> legs) {
        this.legs = legs;
    }

    public GoogleOverviewPolyline getOverviewPolyline() {
        return overviewPolyline;
    }

    public void setOverviewPolyline(GoogleOverviewPolyline overviewPolyline) {
        this.overviewPolyline = overviewPolyline;
    }

    @Override
    public String toString(){
        return new com.google.gson.Gson().toJson(this);
    }
}
