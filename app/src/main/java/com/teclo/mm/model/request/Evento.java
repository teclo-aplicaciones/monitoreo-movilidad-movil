package com.teclo.mm.model.request;

import com.google.gson.annotations.SerializedName;

public class Evento {

    @SerializedName("idEvento")
    private Long idEvento;

    @SerializedName("idTipoEvento")
    private Long idTipoEvento;

    @SerializedName("idSubtipoDispositivo")
    private String idSubtipoDispositivo;

    @SerializedName("cdEvento")
    private String cdEvento;

    @SerializedName("nbEvento")
    private String nbEvento;

    @SerializedName("txEvento")
    private String txEvento;

    @SerializedName("txParametro")
    private String txParametro;

    @SerializedName("stActivo")
    private Boolean stActivo;

    public Boolean getStActivo() {
        return stActivo;
    }

    public void setStActivo(Boolean stActivo) {
        this.stActivo = stActivo;
    }

    public String getTxParametro() {
        return txParametro;
    }

    public void setTxParametro(String txParametro) {
        this.txParametro = txParametro;
    }

    public Long getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(Long idEvento) {
        this.idEvento = idEvento;
    }

    public Long getIdTipoEvento() {
        return idTipoEvento;
    }

    public void setIdTipoEvento(Long idTipoEvento) {
        this.idTipoEvento = idTipoEvento;
    }

    public String getIdSubtipoDispositivo() {
        return idSubtipoDispositivo;
    }

    public void setIdSubtipoDispositivo(String idSubtipoDispositivo) {
        this.idSubtipoDispositivo = idSubtipoDispositivo;
    }

    public String getCdEvento() {
        return cdEvento;
    }

    public void setCdEvento(String cdEvento) {
        this.cdEvento = cdEvento;
    }

    public String getNbEvento() {
        return nbEvento;
    }

    public void setNbEvento(String nbEvento) {
        this.nbEvento = nbEvento;
    }

    public String getTxEvento() {
        return txEvento;
    }

    public void setTxEvento(String txEvento) {
        this.txEvento = txEvento;
    }
}
