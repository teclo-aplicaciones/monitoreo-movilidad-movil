package com.teclo.mm.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class MessageMod extends RealmObject {

    public MessageMod(){}

    public MessageMod(String from, String text, String date, String level) {
        this.from = from;
        this.text = text;
        this.date = date;
        this.level = level;
    }

    @PrimaryKey
    private int idRealm;

    private String from;

    private String text;

    private String date;

    private String level;


    public int getIdRealm() {
        return idRealm;
    }

    public void setIdRealm(int idRealm) {
        this.idRealm = idRealm;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
