package com.teclo.mm.model.response;

import com.google.gson.annotations.SerializedName;
import com.teclo.mm.model.request.Evento;

import java.util.List;

public class EventoResponse {

    @SerializedName("status")
    private Status status;
    @SerializedName("data")
    private List<Evento> data;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<Evento> getData() {
        return data;
    }

    public void setData(List<Evento> data) {
        this.data = data;
    }
}
