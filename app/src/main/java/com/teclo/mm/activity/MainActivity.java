package com.teclo.mm.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.BuildConfig;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.teclo.mm.R;
import com.teclo.mm.Receiver.EventsReceiver;
import com.teclo.mm.commons.Constants;
import com.teclo.mm.fragment.CoordinatesFragment;
import com.teclo.mm.fragment.FragmentDrawer;
import com.teclo.mm.fragment.FriendsFragment;
import com.teclo.mm.fragment.MapsFragment;
import com.teclo.mm.fragment.MessagesFragment;
import com.teclo.mm.model.MessageMod;
import com.teclo.mm.util.PreferenceManager;
import com.teclo.mm.util.RealmManager;
import com.teclo.mm.util.Utils;

import java.lang.reflect.Method;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        MessagesFragment.ActionsFromMessage { //, FriendsFragment.ActionsFromFriends {

    private static String TAG = MainActivity.class.getSimpleName();

    private Toolbar mToolbar;
    private FragmentDrawer drawerFragment;

    private LocationManager mLocationManager;
    private AlertDialog alert;

    private Boolean onResum = false;
    private Boolean onSto = false;

    private EventsReceiver eventsReceiver;
    private RealmManager realmManager;

    private boolean hideTime = true, hideRemove = true;

    ActionBarDrawerToggle toggle;
    DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        eventsReceiver = new EventsReceiver(this);

        setContentView(R.layout.activity_main);

        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        drawer = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, 0); // this disables the animation
            }
        };

        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
                    Log.d(TAG, "No hay fragments");
                } else {
                    getSupportFragmentManager().popBackStack();
                }

                //hide back arrow
                if(getSupportActionBar()!=null) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                    getSupportActionBar().setTitle(getString(R.string.title_messages));
                }
                //shows hamburger menu and prevents View.OnClickListener to be called
                toggle.setDrawerIndicatorEnabled(true);

                hideRemove = false;
                supportInvalidateOptionsMenu();
            }
        });

        drawer.addDrawerListener(toggle);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setCheckedItem(R.id.nav_coordenadas);
        navigationView.setNavigationItemSelectedListener(this);

        realmManager = new RealmManager();
        View header = navigationView.getHeaderView(0);
        TextView mUserName = header.findViewById(R.id.userName);

        /*   Se comento la linea ya que no se logguea un usuario y por ende no hay informacion de este */
        //mUserName.setText(realmManager.getUserModel().getUsername());
        mUserName.setText(R.string.usuarioInvitado);

        //drawerFragment = (FragmentDrawer) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        //drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        //drawerFragment.setDrawerListener(this);

        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if(!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            alertGPS();
        }
        FirebaseInstanceId.getInstance().getToken();
    }

    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }

    protected void onResume(){
        super.onResume();
        onResum = true;

        if(!displayMessageFragment()){
            if(checkPermissions()){
                run();
            }else{
                askPermission();
            }
        }
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        try {
            unregisterReceiver(eventsReceiver);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if(alert != null){
            alert.dismiss();
        }
    }

    @Override
    protected void onStop(){
        super.onStop();
        onSto = true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.action_settings:
                showChangeTimeDialog();
                return true;
            case android.R.id.home:
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        displayView(item.getItemId());
        return true;
    }

    /*@Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        //if (requestCode == Constants.PETICION_PERMISO_LOCALIZACION) {
            if (grantResults.length > 0) {
                boolean locationPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                boolean readphonePermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                if (locationPermission && readphonePermission) {
                    displayView(R.id.nav_coordenadas);
                    //eventsReceiver = new EventsReceiver(this);
                } else {
                    Snackbar.make(
                            findViewById(R.id.drawer_layout),
                            R.string.permission_denied_explanation,
                            Snackbar.LENGTH_INDEFINITE)
                            .setAction(R.string.settings, new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    // Build intent that displays the App settings screen.
                                    Intent intent = new Intent();
                                    intent.setAction(
                                            Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package",
                                            BuildConfig.APPLICATION_ID, null);
                                    intent.setData(uri);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }
                            })
                            .show();
                }
            } else {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            }
        //}
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if(hideTime) {
            menu.findItem(R.id.action_settings).setVisible(false);
        }else{
            menu.findItem(R.id.action_settings).setVisible(true);
        }

        if(hideRemove){
            menu.findItem(R.id.action_remove_messages).setVisible(false);
        }else{
            menu.findItem(R.id.action_remove_messages).setVisible(true);
        }

        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public void onBackPressed(){
        getLogOut();
    }

    private void getLogOut(){
        SharedPreferences sharedpreferences = PreferenceManager.sharedInstance(this).getPackagePreferences();

        if(sharedpreferences.getBoolean(Constants.KEY_REQUESTING_LOCATION_UPDATES, false)){
            Toast.makeText(MainActivity.this, getString(R.string.stop_location_service), Toast.LENGTH_SHORT).show();
        }else {
            finish();
            /*
            new AlertDialog.Builder(this, R.style.AlertDialogCustom)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(getString(R.string.log_out))
                    .setMessage(getString(R.string.confirm_log_out))
                    .setPositiveButton(getString(R.string.generic_positive_answer), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(MainActivity.this, LoginActivity.class);

                            RealmManager realmManager = new RealmManager();
                            if (realmManager.isUserLoggedIn()) {
                                realmManager.removeAll();
                            }

                            startActivity(intent);
                            finish();
                        }

                    })
                    .setNegativeButton(getString(R.string.generic_negative_answer), null)
                    .show();*/
        }
    }

    public void showChangeTimeDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog_time, null);
        dialogBuilder.setView(dialogView);

        final EditText edt = (EditText) dialogView.findViewById(R.id.edit1);

        SharedPreferences sharedPreferences = PreferenceManager.sharedInstance(this).getPackagePreferences();
        Long minutes = sharedPreferences.getLong(Constants.KEY_INTERVAL_LOCATION_UPDATE, 60000L) / 60000L;
        edt.setText(String.valueOf(minutes));

        dialogBuilder.setTitle(getString(R.string.title_alert_time_location_update));
        dialogBuilder.setMessage(getString(R.string.message_alert_time_location_update));
        dialogBuilder.setPositiveButton(getString(R.string.generic_save), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                milisecondsRequestUpdate(edt.getText().toString());
            }
        });
        dialogBuilder.setNegativeButton(getString(R.string.generic_not_save), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    private void milisecondsRequestUpdate(String minutos){
        Long minutes = Long.valueOf(minutos);
        Long miliseconds = minutes * 60000L;

        SharedPreferences.Editor edit = PreferenceManager.sharedInstance(this).getPackagePreferences().edit();
        edit.putLong(Constants.KEY_INTERVAL_LOCATION_UPDATE, miliseconds);
        edit.commit();
    }

    private void run(){
        SharedPreferences sharedPreferences = PreferenceManager.sharedInstance(this).getPackagePreferences();
        SharedPreferences.Editor edit = sharedPreferences.edit();

        String id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        //String serialNumber = getDeviceSerial();
        edit.putString(Constants.DEVICE_ID, id);
        edit.apply();

        displayView(sharedPreferences.getInt(Constants.FRAGMENT_LAUNCH_SERVICE,R.id.nav_coordenadas));
    }

    private void alertGPS(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("El sistema GPS esta desactivado, ¿Desea activarlo?")
                .setCancelable(false)
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alert = builder.create();
        alert.show();
    }

    private String getDeviceSerial(){
        String serialNumber = "";
        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("get", String.class, String.class);

            serialNumber = (String) get.invoke(c, "sys.serialnumber", "unknown");
            if (serialNumber.equals("unknown")) {
                serialNumber = (String) get.invoke(c, "ril.serialnumber", "unknown");

                if (serialNumber.equals("unknown")) {
                    serialNumber = (String) get.invoke(c, "ro.serialno", "unknown");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return serialNumber;
    }

    private void displayView(int id) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);
        SharedPreferences sharedpreferences = PreferenceManager.sharedInstance(this).getPackagePreferences();

        if(onResum && !onSto) {
            if (sharedpreferences.getBoolean(Constants.KEY_REQUESTING_LOCATION_UPDATES, false)) {
                Toast.makeText(MainActivity.this, getString(R.string.stop_location_service), Toast.LENGTH_SHORT).show();
                return;
            }
        }

        hideTime = true; hideRemove = true;
        onSto = false;
        SharedPreferences.Editor edit = sharedpreferences.edit();
        switch (id) {
            case R.id.nav_coordenadas:
                fragment = new CoordinatesFragment();
                title = getString(R.string.title_coordinates);
                edit.putInt(Constants.FRAGMENT_LAUNCH_SERVICE, R.id.nav_coordenadas);
                hideTime = false;
                break;
            case R.id.nav_map:
                fragment = new MapsFragment();
                title = getString(R.string.title_routes);
                edit.putInt(Constants.FRAGMENT_LAUNCH_SERVICE, R.id.nav_map);
                hideTime = false;
                break;
            //case R.id.nav_logout:
                /*  Se cancela esta funcion ya que no hay inicio de session  */
                // getLogOut();
                //fragment = new MessagesFragment();
                //title = getString(R.string.title_messages);
            //    break;
            default:
                break;
        }

        supportInvalidateOptionsMenu();

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commitAllowingStateLoss();

            getSupportActionBar().setTitle(title);
            edit.commit();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    public Boolean checkPermissions(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int FirstPermissionResult = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            int SecondPermissionResult = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
            return FirstPermissionResult == PackageManager.PERMISSION_GRANTED && SecondPermissionResult == PackageManager.PERMISSION_GRANTED;
            //return (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);
        }else{
            return true;
        }
    }

    public void askPermission(){
        //devuelve false solo si el usuario ha seleccionado Nunca preguntar de nuevo o la directiva de dispositivo prohíbe que la aplicación tenga permiso
        boolean shouldProvideRationaleLocation = ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION);
        boolean shouldProvideRationaleReadPhone = ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationaleLocation || shouldProvideRationaleReadPhone) {
            Snackbar.make(findViewById(R.id.drawer_layout),
                    R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.permission_rationale_ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                                    Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.READ_PHONE_STATE
                            }, Constants.PETICION_PERMISO_LOCALIZACION);
                        }
                    })
                    .show();
        } else {
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.READ_PHONE_STATE
            }, Constants.PETICION_PERMISO_LOCALIZACION);
        }
    }

    public boolean displayMessageFragment(){
        if(getIntent().getExtras() != null){
            String openFrag = getIntent().getStringExtra("fragment"); //click_action

            if(openFrag.equals(CoordinatesFragment.TAG)){
                displayView(R.id.nav_coordenadas);
                return true;
            }else if(openFrag.equals(MapsFragment.TAG)){
                displayView(R.id.nav_map);
                return true;
            }else if(openFrag.equals(MessagesFragment.TAG)){
                String message = null, title = "";
                title = getIntent().getStringExtra("title");
                message = getIntent().getStringExtra("body");

                if (message != null) {
                    if(getIntent().getStringExtra(Constants.NOTIFICATION_MESSAGE_KEY) == null){
                        realmManager.saveMessage(new MessageMod(title, message, Utils.dateFormat("dd/MM/yyyy HH:mm:ss"), "Warn"));
                    }
                    Fragment fragment = MessagesFragment.newInstance(message);
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.container_body, fragment, MessagesFragment.TAG); //commitAllowingStateLoss
                    transaction.addToBackStack(MessagesFragment.TAG);
                    transaction.commit();
                    getSupportActionBar().setTitle(getString(R.string.title_messages));

                    hideTime = true; hideRemove = false;
                    supportInvalidateOptionsMenu();

                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void goBack() {
        SharedPreferences sharedPreferences = PreferenceManager.sharedInstance(this).getPackagePreferences();
        displayView(sharedPreferences.getInt(Constants.FRAGMENT_LAUNCH_SERVICE,R.id.nav_coordenadas));
    }

    @Override
    public void goDetailMessage(MessageMod message) {
        Fragment fragment = FriendsFragment.newInstance(message.getText(), message.getDate());
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container_body, fragment, FriendsFragment.TAG);
        transaction.addToBackStack(FriendsFragment.TAG);
        transaction.commit(); //commitAllowingStateLoss

        toggle.setDrawerIndicatorEnabled(false);
        //show back arrow
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(message.getFrom());
        }

        hideTime = true; hideRemove = true;
        supportInvalidateOptionsMenu();
    }
}