package com.teclo.mm.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.auth0.android.jwt.JWT;
import com.teclo.mm.R;
import com.teclo.mm.commons.Constants;
import com.teclo.mm.model.request.UserMod;
import com.teclo.mm.model.response.UserResponseMod;
import com.teclo.mm.util.ApiUtilsRetrofit;
import com.teclo.mm.util.PreferenceManager;
import com.teclo.mm.util.RealmManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    public final static String TAG = LoginActivity.class.getSimpleName();

    //UI Elements
    ProgressDialog progressDialog;
    EditText mPassword;
    EditText mUser;
    Button mLogin;
    TextInputLayout mTextUser;
    TextInputLayout mTextPassword;

    private static final int REQUEST_SIGNUP = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mUser  = (EditText) findViewById(R.id.input_user);
        mPassword  = (EditText) findViewById(R.id.input_password);
        mLogin = (Button) findViewById(R.id.btn_login);

        mTextUser = (TextInputLayout) findViewById(R.id.txt_user);
        mTextPassword = (TextInputLayout) findViewById(R.id.txt_password);

        SharedPreferences.Editor edit = PreferenceManager.sharedInstance(this).getPackagePreferences().edit();
        edit.clear();
        edit.commit();
        //Utils.removeAllPreferences(LoginActivity.this);

        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    private void login(){
        if(!validate()){
            onLoginFailed(getString(R.string.requirement_data_toast));
            return;
        }

        mLogin.setEnabled(false);
        String user = mUser.getText().toString();
        String password = mPassword.getText().toString();

        progressDialog = new ProgressDialog(LoginActivity.this, ProgressDialog.STYLE_SPINNER); //R.style.AppTheme_Dark_Dialog
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.login_dialog_message));
        progressDialog.show();

        UserMod userObj = new UserMod(user, password);
        ApiUtilsRetrofit.getUsersService().getLogin(userObj).enqueue(new Callback<UserResponseMod>() {
            @Override
            public void onResponse(Call<UserResponseMod> call, Response<UserResponseMod> response) {

                if(response.isSuccessful()) {
                    UserResponseMod user = response.body();
                    onLoginSuccess(user);
                }else {
                    //int statusCode  = response.code();
                    onLoginFailed(response.message());
                    //Toast.makeText(LoginActivity.this, , Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<UserResponseMod> call, Throwable t) {
                Log.e(TAG, t.toString());
                onLoginFailed(getString(R.string.server_error));
                progressDialog.dismiss();
            }
        });
    }

    private boolean validate(){
        boolean valid = true;

        String user = mUser.getText().toString();
        String password = mPassword.getText().toString();

        if(user.isEmpty()){
            toggleTextInputLayoutError(mTextUser, getString(R.string.requirement_data_controls));
            valid = false;
        }else{
            toggleTextInputLayoutError(mTextUser, null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            toggleTextInputLayoutError(mTextPassword, getString(R.string.password_invalid));
            valid = false;
        } else {
            toggleTextInputLayoutError(mTextPassword, null);
        }

        return valid;
    }

    private void toggleTextInputLayoutError(@NonNull TextInputLayout textInputLayout, String msg) {
        if (msg == null) {
            textInputLayout.setErrorEnabled(false);
        } else {
            textInputLayout.setErrorEnabled(true);
        }
        textInputLayout.setError(msg);
    }

    public void onLoginSuccess(UserResponseMod user) {
        mLogin.setEnabled(true);

        RealmManager realmManager = new RealmManager();
        JWT jwt = new JWT(user.getToken());

        user.setUser(jwt.getClaim("sub").asString());
        user.setUsername(jwt.getClaim("name").asString());
        realmManager.setUserModel(user);

        SharedPreferences.Editor editor = PreferenceManager.sharedInstance(LoginActivity.this).getPackagePreferences().edit();
        editor.putString(Constants.LOGIN_TOKEN, user.getToken());
        editor.commit();

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        finish();
    }

    public void onLoginFailed(String message) {
        Toast.makeText(getBaseContext(), message, Toast.LENGTH_LONG).show();
        mLogin.setEnabled(true);
    }

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {

                // TODO: Implement successful signup logic here
                // By default we just finish the Activity and log them in automatically
                this.finish();
            }
        }
    }*/
}
