package com.teclo.mm.service;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.google.firebase.messaging.RemoteMessage;
import com.teclo.mm.R;
import com.teclo.mm.activity.MainActivity;
import com.teclo.mm.commons.Constants;
import com.teclo.mm.fragment.MessagesFragment;
import com.teclo.mm.model.MessageMod;
import com.teclo.mm.model.response.Status;
import com.teclo.mm.util.ApiUtilsRetrofit;
import com.teclo.mm.util.RealmManager;
import com.teclo.mm.util.Utils;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;


/**
 * Created by Fjmb on 01/10/2017.
 */
public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {

    NotificationChannel channel = null;
    RealmManager realmManager = null;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Log.e("NotificacionData", "NotificacionData: " + remoteMessage.getNotification());
        //Log.e("DataData", "DataData: " + remoteMessage.getData());
        //Log.e("dataChat",remoteMessage.getData().get("message"));
        //Log.e("Test", "Message Notification Body: " + remoteMessage.getNotification().getBody());
        //Log.e("dataChat", remoteMessage.getFrom());

        Log.e("SMB", remoteMessage.getFrom());
        if (remoteMessage.getData().size() > 0) {
            Log.e("dataChat", "MessageDataPayload, " + remoteMessage.getData());
            String action = remoteMessage.getData().get(Constants.CMD_ACTION);
            if (TextUtils.isEmpty(action)) {
                if (realmManager == null) {
                    realmManager = new RealmManager();
                }

                realmManager.saveMessage(new MessageMod(remoteMessage.getData().get("title"), remoteMessage.getData().get(Constants.CMD_BODY), Utils.dateFormat("dd/MM/yyyy HH:mm:ss"), "Warn"));
                showNotification(remoteMessage.getData());
                sendBroadcastMessage(remoteMessage.getData().get("title"), remoteMessage.getData().get(Constants.CMD_BODY));
            } else {
                executeActions(action,remoteMessage.getData().get(Constants.CMD_BODY));
            }
        } else {
            Log.e("dataChat", "Recibi algo pero no hay datos");
        }
    }

    private void showNotification(Map<String, String> remoteMessage) {
        String CHANNEL_ID = "1";
        String title = remoteMessage.get("title");
        String message = remoteMessage.get("body");
        String icono = remoteMessage.get("image"); //String fragment = remoteMessage.get("click_action");
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        intent.putExtra(Constants.NOTIFICATION_MESSAGE_KEY, "");
        for (String key : remoteMessage.keySet()) {
            Object value = remoteMessage.get(key);
            intent.putExtra(key, (String) value);
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "")
                .setLargeIcon(decodeImage(icono))/*Notification icon image*/
                .setSmallIcon(R.drawable.logomm)
                .setContentTitle(title)
                .setContentText(message)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);
                /*.setStyle(new NotificationCompat.BigPictureStyle()
                        .bigPicture(decodeImage(icono)))*/
        //Seteamos el intent
        //.setFullScreenIntent(pendingIntent, true)


        //NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationManagerCompat manager = NotificationManagerCompat.from(this);
        manager.notify(0, builder.build());
    }

    private void sendBroadcastMessage(String title, String message) {
        Intent intent = new Intent();
        intent.putExtra("title", title);
        intent.putExtra("body", message);
        intent.setAction(MessagesFragment.BROADCAST);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    private void executeActions(String action,String body) {
        switch (action) {
            case Constants.SOUND_ON_ACTION:
                sound(body);
                break;
            case Constants.SOUND_OFF_ACTION:
                sound(body);
                break;
            case Constants.WIFI_ON_ACTION:
                wifi(body,true);
                break;
            case Constants.WIFI_OFF_ACTION:
                wifi(body,false);
                break;
            case Constants.GPS_ACTION:
                gpsActual(body);
                break;
            case Constants.MOBILE_DATA_ACTION:
                mobile(this, false);
                break;
        }
    }

    private void sound(String body) {
        final AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        //final int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_NOTIFICATION);
        final int currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_NOTIFICATION);
        if (currentVolume < 1) {
            audioManager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, audioManager.getStreamMaxVolume(AudioManager.STREAM_NOTIFICATION), 0);
        }
        //MediaPlayer player = MediaPlayer.create(this, Settings.System.DEFAULT_RINGTONE_URI);
        MediaPlayer player = new MediaPlayer();
        try {
            player.setDataSource(this, Settings.System.DEFAULT_RINGTONE_URI);
        } catch (Exception e1) {
            player.release();
            return;
        }
        player.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                mediaPlayer.release();
                //audioManager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, volume, 0);
            }
        });

        try {
            player.prepare();
        } catch (Exception e1) {
            player.release();
            return;
        }
        player.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
            @Override
            public void onSeekComplete(MediaPlayer mediaPlayer) {
                mediaPlayer.stop();
                mediaPlayer.start();
            }
        });
        //player.setVolume(maxVolume, maxVolume);
        player.start();
        /*Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        Ringtone ringtone = RingtoneManager.getRingtone(this,uri);
        ringtone.play();*/

        ApiComandoBitacora apiComandoBitacora = ApiUtilsRetrofit.getComandoBitacora();
        Call<Status> call = apiComandoBitacora.comandoBitacora(Long.parseLong(body),"RECIBIDO: "+" se ejecuto alerta");
        try {
            Response<Status> rp  =  call.execute();
        } catch (IOException e) {
        }
    }

    private void wifi(String body,boolean onOff) {
        WifiManager wifiManager = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);
        String msg="";
        if (wifiManager != null) {
            if (onOff){
                wifiManager.setWifiEnabled(true);
                msg ="WIFI Prendido";
            }else if (!onOff){
                wifiManager.setWifiEnabled(false);
                msg ="WIFI Apagado";
            }
        }

        ApiComandoBitacora apiComandoBitacora = ApiUtilsRetrofit.getComandoBitacora();
        Call<Status> call = apiComandoBitacora.comandoBitacora(Long.parseLong(body),"RECIBIDO: "+msg);
        try {
            Response<Status> rp  =  call.execute();
        } catch (IOException e) {
        }
    }

    private void gpsActual(String body) {
        LocationManager locationManager =  (LocationManager) getSystemService(this.LOCATION_SERVICE);
        String msg = "";

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        } else {

            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            Location location1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location location2 = locationManager.getLastKnownLocation(LocationManager. PASSIVE_PROVIDER);
            if (location != null) {
                msg = "NETWORK_PROVIDER Lattitude = " + location.getLatitude() + " Longitude = " + location.getLongitude();
            } else  if (location1 != null) {
                msg = "GPS_PROVIDER Lattitude = " + location1.getLatitude() + " Longitude = " + location1.getLongitude();
            } else  if (location2 != null) {
                msg = "PASSIVE_PROVIDER Lattitude = " + location2.getLatitude() + " Longitude = " + location2.getLongitude();
            }
        }

        ApiComandoBitacora apiComandoBitacora = ApiUtilsRetrofit.getComandoBitacora();
        Call<Status> call = apiComandoBitacora.comandoBitacora(Long.parseLong(body),"RECIBIDO: "+msg);
        try {
            Response<Status> rp  =  call.execute();
        } catch (IOException e) {
        }
    }

    private boolean gps(){

        PackageManager pacman = getPackageManager();
        PackageInfo pacInfo = null;

        try {
            pacInfo = pacman.getPackageInfo("com.android.settings", PackageManager.GET_RECEIVERS);
        } catch (PackageManager.NameNotFoundException e) {
            return false; //package not found
        }

        if(pacInfo != null){
            for(ActivityInfo actInfo : pacInfo.receivers){
                //test if recevier is exported. if so, we can toggle GPS.
                if(actInfo.name.equals("com.android.settings.widget.SettingsAppWidgetProvider") && actInfo.exported){
                    return true;
                }
            }
        }

        return false; //default

    }

    private void mobile(Context context, boolean enabled){
        if (Build.VERSION.SDK_INT >= 21) {
            try {
                TelephonyManager telephonyService = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                Method setMobileDataEnabledMethod = telephonyService.getClass().getDeclaredMethod("setDataEnabled", boolean.class);
                if (null != setMobileDataEnabledMethod) {
                    setMobileDataEnabledMethod.invoke(telephonyService, enabled);
                }
                return;
            } catch (Exception ex) {
                return;
            }
        } /*else {
            try {
                final ConnectivityManager conman = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                final Class conmanClass = Class.forName(conman.getClass().getName());
                final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
                iConnectivityManagerField.setAccessible(true);
                final Object iConnectivityManager = iConnectivityManagerField.get(conman);
                final Class iConnectivityManagerClass = Class.forName(iConnectivityManager.getClass().getName());
                final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
                setMobileDataEnabledMethod.setAccessible(true);
                setMobileDataEnabledMethod.invoke(iConnectivityManager, enabled);
                return;
            } catch (NoSuchMethodException e) {
                final ConnectivityManager conman = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                Class conmanClass;
                try {
                    conmanClass = Class.forName(conman.getClass().getName());
                    final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
                    iConnectivityManagerField.setAccessible(true);
                    final Object iConnectivityManager = iConnectivityManagerField.get(conman);
                    final Class iConnectivityManagerClass = Class.forName(iConnectivityManager.getClass().getName());
                    Class[] cArg = new Class[2];
                    cArg[0] = String.class;
                    cArg[1] = Boolean.TYPE;
                    Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", cArg);
                    Object[] pArg = new Object[2];
                    pArg[0] = context.getPackageName();
                    pArg[1] = enabled;
                    setMobileDataEnabledMethod.setAccessible(true);
                    setMobileDataEnabledMethod.invoke(iConnectivityManager, pArg);
                    return;
                } catch (Exception e1) {
                    e1.printStackTrace();
                    return;
                }
            } catch (Exception e) {
                return;
            }
        }*/
    }

    /*private void createNotificationChannel(String CHANNEL_ID) {
        /*
        Create the NotificationChannel, but only on API 26+ because
        the NotificationChannel class is new and not in the support library

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            if (channel != null) {
                channel = null;
                notificationManager.deleteNotificationChannel(CHANNEL_ID);
                createNotificationChannel(CHANNEL_ID);
            } else {
                CharSequence name = getString(R.string.channel_name);
                String description = getString(R.string.channel_desc);
                //int importance = NotificationManager.IMPORTANCE_DEFAULT;
                int importance = NotificationManager.IMPORTANCE_HIGH;
                channel = new NotificationChannel(CHANNEL_ID, name, importance);
                channel.setDescription(description);
                // Register the channel with the system; you can't change the importance
                // or other notification behaviors after this

                notificationManager.createNotificationChannel(channel);
            }
        }*/


    private static Bitmap decodeImage(String image_data) {
        // Decode the encoded string into largeIcon
        Bitmap largeIcon = null;
        if ((image_data != null) && (!image_data.equals(""))) {
            byte[] decodedImage = Base64.decode(image_data, Base64.DEFAULT);
            if (decodedImage != null) {
                largeIcon = BitmapFactory.decodeByteArray(decodedImage
                        , 0
                        , decodedImage.length);
            }
        }
        return largeIcon;
    }
}