package com.teclo.mm.service;

import com.teclo.mm.model.request.LocationMod;
import com.teclo.mm.model.request.PosicionesAct;
import com.teclo.mm.model.request.RouteMod;
import com.teclo.mm.model.response.PosicionesActResponse;
import com.teclo.mm.model.response.RouteResponseMod;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Jesus Gutierrez on 18/10/2017.
 */

public interface ApiLocation {

    //movil/coordenadas
    @POST("HangHeld/coordenada")
    Call<LocationMod> postCoordenada(@Body LocationMod locationMod);

    @POST("movil/rutas")
    Call<RouteResponseMod> postRuta(@Body RouteMod routeMod);

    @POST("coordenada")
    Call<PosicionesActResponse> addCoordenada(@Body PosicionesAct posicionesAct);
}
