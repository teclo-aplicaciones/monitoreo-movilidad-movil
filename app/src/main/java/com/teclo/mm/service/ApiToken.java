package com.teclo.mm.service;

import com.teclo.mm.model.response.DeviceFirebase;

import retrofit2.Call;
import retrofit2.http.PUT;
import retrofit2.http.Query;

/**
 * Created by Unitis007 on 18/10/2018.
 */

public interface ApiToken {

    @PUT("dispositivos/firebase/token")
    Call<DeviceFirebase> updateToken(@Query("nuImei") Long imei, @Query("tokenFirebase") String token);

}
