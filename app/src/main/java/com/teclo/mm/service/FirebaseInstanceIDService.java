package com.teclo.mm.service;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.teclo.mm.commons.Constants;
import com.teclo.mm.util.AsyncTaskToken;
import com.teclo.mm.util.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;



/**
 * Created by Fjmb on 01/10/2017.
 */
public class FirebaseInstanceIDService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        String token = FirebaseInstanceId.getInstance().getToken();
        registerToken(token);
    }

    private void registerToken(String token) {
        Log.i("SMB",token);
        SharedPreferences sharedPreferences = PreferenceManager.sharedInstance(getApplicationContext()).getPackagePreferences();
        Long imei = sharedPreferences.getLong(Constants.NU_IMEI, 0L);
        if(!imei.equals(0L))
            new AsyncTaskToken(sharedPreferences.getLong(Constants.NU_IMEI, 0L), token).execute();

        //Constants.NU_IMEI;
        //http://desktop-5u1u7mh:9080/teclocdmxmonimovi-wsw_v100r2/dispositivos/firebase/token?nuImei=352632083095277&tokenFirebase=111111
    }
}
