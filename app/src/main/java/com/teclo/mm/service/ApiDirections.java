package com.teclo.mm.service;

import com.teclo.mm.model.Google.GoogleResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Jesus Gutierrez on 18/10/2017.
 */

public interface ApiDirections {

    @GET("directions/json")
    Call<GoogleResponse> getGoogleDirection(@Query("origin") String origin, @Query("destination") String destination,
                                            @Query("mode") String mode, @Query("language") String language,
                                            @Query("key") String key);
}
