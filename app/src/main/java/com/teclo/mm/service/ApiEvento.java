package com.teclo.mm.service;

import com.teclo.mm.model.response.EventoResponse;
import com.teclo.mm.model.response.ReporteEventoResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiEvento {

    @GET("eventos")
    Call<EventoResponse> getEventos();

}
