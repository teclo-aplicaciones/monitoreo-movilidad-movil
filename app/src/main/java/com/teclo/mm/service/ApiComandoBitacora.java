package com.teclo.mm.service;

import com.teclo.mm.model.response.DeviceFirebase;
import com.teclo.mm.model.response.Status;

import retrofit2.Call;
import retrofit2.http.PUT;
import retrofit2.http.Query;

/**
 * Created by Unitis007 on 18/10/2018.
 */

public interface ApiComandoBitacora {

    @PUT("comando/bitacora")
    Call<Status> comandoBitacora(@Query("idBitacora") Long idBitacora, @Query("txBitacoraDetalle") String txBitacoraDetalle);

}
