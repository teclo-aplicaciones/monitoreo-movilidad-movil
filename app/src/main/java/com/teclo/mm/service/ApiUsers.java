package com.teclo.mm.service;

import com.teclo.mm.model.request.UserMod;
import com.teclo.mm.model.response.UserResponseMod;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Jesus Gutierrez on 06/10/2017.
 */

public interface ApiUsers {

    @POST("login")
    Call<UserResponseMod> getLogin(@Body UserMod userMod);
}
