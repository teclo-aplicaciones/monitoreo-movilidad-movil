package com.teclo.mm.Receiver;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.teclo.mm.commons.Constants;
import com.teclo.mm.model.request.Evento;
import com.teclo.mm.model.request.PosicionesAct;
import com.teclo.mm.model.response.EventoResponse;
import com.teclo.mm.model.response.PosicionesActResponse;
import com.teclo.mm.util.ApiUtilsRetrofit;
import com.teclo.mm.util.PreferenceManager;
import com.teclo.mm.util.RealmManager;
import com.teclo.mm.util.Utils;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.CONNECTIVITY_SERVICE;

public class EventsReceiver extends BroadcastReceiver {

    private static final String TAG = EventsReceiver.class.getSimpleName();
    private static final String currentDate = Utils.dateFormat("yyyy/MM/dd HH:mm:ss");

    private EventoResponse eventoResponse;
    private HashMap<Long, Evento> mapEventos;
    private SharedPreferences sharedPreferences;

    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private LocationRequest mLocationRequest;
    private Location locationNow;

    private RealmManager realmManager;

    private enum ID_EVENTO {

        TIEMPO(1L),
        INACTIVIDAD(2L),
        BATERIA30(3L),
        BATERIA15(4L),
        NETWORK_4G(5L),
        NETWORK_3G(6L),
        NETWORK_2G(7L),
        GPS_ON_SOLO_DISPOSITIVO(8L),
        GPS_ON_AHORRO_BATERIA(14L),
        GPS_ON_ALTA_PRECISION(15L),
        GPS_OFF(9L),
        WIFI_ON(10L),
        WIFI_OFF(11L),
        MOBILE_DATA_ON(12L),
        MOBILE_DATA_OFF(13L);

        private Long id;

        private ID_EVENTO(Long id) {
            this.id = id;
        }

        public Long getId() {
            return this.id;
        }
    }


    public EventsReceiver(Context context) {
        context.registerReceiver(this, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        context.registerReceiver(this, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));
        context.registerReceiver(this, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        context.registerReceiver(this, new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION));

        sharedPreferences = PreferenceManager.sharedInstance(context).getPackagePreferences();
        setIP();
        setImei(context);
        setLocation(context);
        realmManager = new RealmManager(context);

        ApiUtilsRetrofit.getEventoService().getEventos().enqueue(new Callback<EventoResponse>() {
            @Override
            public void onResponse(Call<EventoResponse> call, Response<EventoResponse> response) {
                if (response.isSuccessful()) {
                    eventoResponse = response.body();

                    mapEventos = new HashMap<>();
                    for (Evento e : eventoResponse.getData()) {
                        mapEventos.put(e.getIdEvento(), e);
                    }

                }
            }

            @Override
            public void onFailure(Call<EventoResponse> call, Throwable t) {
            }
        });
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        switch (intent.getAction()) {

            case Intent.ACTION_BATTERY_CHANGED:
                int battery30 = 30;
                int battery15 = 15;
                boolean isBattery30 = true;
                boolean isBattery15 = true;

                if (eventoResponse != null) {
                    battery30 = Integer.parseInt(mapEventos.get(ID_EVENTO.BATERIA30.getId()).getTxParametro());
                    isBattery30 = mapEventos.get(ID_EVENTO.BATERIA30.getId()).getStActivo();

                    battery15 = Integer.parseInt(mapEventos.get(ID_EVENTO.BATERIA15.getId()).getTxParametro());
                    isBattery15 = mapEventos.get(ID_EVENTO.BATERIA15.getId()).getStActivo();
                }

                if (isBattery15 && isBattery30) {
                    int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
                    if (level > battery15 && level <= battery30) {
                        postPosicionesAct(ID_EVENTO.BATERIA30.getId());
                    } else if (level <= battery15) {
                        postPosicionesAct(ID_EVENTO.BATERIA15.getId());
                    }
                }
                break;

            case LocationManager.PROVIDERS_CHANGED_ACTION:
                LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                boolean isGPSNetwork = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                boolean isGPS = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);

                // Alta Precision
                if (isGPSNetwork && isGPS) {
                    postPosicionesAct(ID_EVENTO.GPS_ON_ALTA_PRECISION.getId());
                }

                // Ahorro de bateria
                else if (isGPSNetwork && isGPS == false) {
                    postPosicionesAct(ID_EVENTO.GPS_ON_AHORRO_BATERIA.getId());
                }

                // Solo en dispositivo
                else if (isGPS) {
                    postPosicionesAct(ID_EVENTO.GPS_ON_SOLO_DISPOSITIVO.getId());
                }

                // off gps
                else if (isGPSNetwork == false && isGPS == false) {
                    postPosicionesAct(ID_EVENTO.GPS_OFF.getId());
                }

                break;

            case WifiManager.WIFI_STATE_CHANGED_ACTION:

                int WifiState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE,
                        WifiManager.WIFI_STATE_UNKNOWN);

                switch (WifiState) {
                    case WifiManager.WIFI_STATE_ENABLED:
                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                pullPostLocalPosicionesAct();
                                postPosicionesAct(ID_EVENTO.WIFI_ON.getId());
                            }
                        }, 10000);
                        break;

                    case WifiManager.WIFI_STATE_DISABLED:
                        postLocalPosicionesAct(ID_EVENTO.WIFI_OFF.getId());
                        break;
                }
                break;

            case ConnectivityManager.CONNECTIVITY_ACTION:
                ConnectivityManager cm = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = cm.getActiveNetworkInfo();

                // off
                if (networkInfo == null) {
                    postLocalPosicionesAct(ID_EVENTO.MOBILE_DATA_OFF.getId());
                }

                // on
                else if (networkInfo.isConnected() && networkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {

                    switch (networkInfo.getSubtype()) {
                        case TelephonyManager.NETWORK_TYPE_GPRS:
                        case TelephonyManager.NETWORK_TYPE_EDGE:
                        case TelephonyManager.NETWORK_TYPE_CDMA:
                        case TelephonyManager.NETWORK_TYPE_1xRTT:
                        case TelephonyManager.NETWORK_TYPE_IDEN:
                            new Handler().postDelayed(new Runnable() {
                                public void run() {
                                    postPosicionesAct(ID_EVENTO.MOBILE_DATA_ON.getId()); // start mobile data
                                    postPosicionesAct(ID_EVENTO.NETWORK_2G.getId()); // 2G
                                }
                            }, 10000);

                            // 2G
                            break;
                        case TelephonyManager.NETWORK_TYPE_UMTS:
                        case TelephonyManager.NETWORK_TYPE_EVDO_0:
                        case TelephonyManager.NETWORK_TYPE_EVDO_A:
                        case TelephonyManager.NETWORK_TYPE_HSDPA:
                        case TelephonyManager.NETWORK_TYPE_HSUPA:
                        case TelephonyManager.NETWORK_TYPE_HSPA:
                        case TelephonyManager.NETWORK_TYPE_EVDO_B:
                        case TelephonyManager.NETWORK_TYPE_EHRPD:
                        case TelephonyManager.NETWORK_TYPE_HSPAP:
                            new Handler().postDelayed(new Runnable() {
                                public void run() {
                                    postPosicionesAct(ID_EVENTO.MOBILE_DATA_ON.getId()); // start mobile data
                                    postPosicionesAct(ID_EVENTO.NETWORK_3G.getId()); // 3G
                                }
                            }, 10000);

                            // 3G
                            break;
                        case TelephonyManager.NETWORK_TYPE_LTE:
                            new Handler().postDelayed(new Runnable() {
                                public void run() {
                                    postPosicionesAct(ID_EVENTO.MOBILE_DATA_ON.getId()); // start mobile data
                                    postPosicionesAct(ID_EVENTO.NETWORK_4G.getId()); // 4G
                                }
                            }, 10000);
                            // 4G
                            break;
                    }

                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            pullPostLocalPosicionesAct();
                        }
                    }, 10000);
                }
                break;
        }

    }

    private void postPosicionesAct(final Long idEvento) {
        requestLocationUpdates();

        final PosicionesAct posicionesAct = new PosicionesAct();
        posicionesAct.setIdDispositivo(1L);
        posicionesAct.setIdEvento(idEvento);
        if (locationNow != null) {
            posicionesAct.setNuLatitud(locationNow.getLatitude());
            posicionesAct.setNuLongitud(locationNow.getLongitude());
            posicionesAct.setNuAltitud(locationNow.getAltitude());
            posicionesAct.setNuAccuracy(locationNow.getAccuracy());
        } else {
            posicionesAct.setNuLatitud(0);
            posicionesAct.setNuLongitud(0);
            posicionesAct.setNuAltitud(0);
            posicionesAct.setNuAccuracy(0);
        }
        posicionesAct.setNuImei(sharedPreferences.getLong(Constants.NU_IMEI, 0L));
        posicionesAct.setNuIp(sharedPreferences.getString(Constants.NU_IP, "0.0.0.0"));
        posicionesAct.setFhRegistro(currentDate);

        Log.i(TAG, posicionesAct.toString());
        ApiUtilsRetrofit.getLocationService().addCoordenada(posicionesAct).enqueue(new Callback<PosicionesActResponse>() {
            @Override
            public void onResponse(Call<PosicionesActResponse> call, Response<PosicionesActResponse> response) {
                if (response.isSuccessful()) {
                    PosicionesActResponse result = response.body();
                    Log.i(TAG, result.toString());
                } else {
                    Log.e(TAG, "Error al enviar json");
                }
            }

            @Override
            public void onFailure(Call<PosicionesActResponse> call, Throwable t) {
                Log.e(TAG, t.toString());
                realmManager.setPosicionesAct(posicionesAct);
            }
        });
    }

    private void postLocalPosicionesAct(final Long idEvento) {
        requestLocationUpdates();
        PosicionesAct posicionesAct = new PosicionesAct();
        posicionesAct.setIdDispositivo(1L);
        posicionesAct.setIdEvento(idEvento);
        if (locationNow != null) {
            posicionesAct.setNuLatitud(locationNow.getLatitude());
            posicionesAct.setNuLongitud(locationNow.getLongitude());
            posicionesAct.setNuAltitud(locationNow.getAltitude());
            posicionesAct.setNuAccuracy(locationNow.getAccuracy());
        } else {
            posicionesAct.setNuLatitud(0);
            posicionesAct.setNuLongitud(0);
            posicionesAct.setNuAltitud(0);
            posicionesAct.setNuAccuracy(0);
        }
        posicionesAct.setNuImei(sharedPreferences.getLong(Constants.NU_IMEI, 0L));
        posicionesAct.setNuIp(sharedPreferences.getString(Constants.NU_IP, "0.0.0.0"));
        posicionesAct.setFhRegistro(currentDate);

        realmManager.setPosicionesAct(posicionesAct);
    }

    private void pullPostLocalPosicionesAct() {
        for (final PosicionesAct p : realmManager.getPosicionesActs()) {

            Log.i(TAG, p.toString());

            PosicionesAct posicionesAct = new PosicionesAct();
            posicionesAct.setIdDispositivo(1L);
            posicionesAct.setIdEvento(p.getIdEvento());
            posicionesAct.setNuLatitud(p.getNuLatitud());
            posicionesAct.setNuLongitud(p.getNuLongitud());
            posicionesAct.setNuAltitud(p.getNuAltitud());
            posicionesAct.setNuAccuracy(p.getNuAccuracy());
            posicionesAct.setNuImei(p.getNuImei());
            posicionesAct.setNuIp(p.getNuIp());
            posicionesAct.setFhRegistro(p.getFhRegistro());

            ApiUtilsRetrofit.getLocationService().addCoordenada(posicionesAct).enqueue(new Callback<PosicionesActResponse>() {
                @Override
                public void onResponse(Call<PosicionesActResponse> call, Response<PosicionesActResponse> response) {
                    if (response.isSuccessful()) {
                        PosicionesActResponse result = response.body();
                        Log.i(TAG, result.toString());
                        realmManager.deletePosicion(p);
                    } else {
                        Log.e(TAG, "Error al enviar json");
                    }
                }

                @Override
                public void onFailure(Call<PosicionesActResponse> call, Throwable t) {
                    Log.e(TAG, t.toString());
                }
            });
        }

    }

    private void setIP() {
        List<InetAddress> addrs;
        String address = "0.0.0.0";
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress() && addr instanceof Inet4Address) {
                        address = addr.getHostAddress().toUpperCase(new Locale("es", "MX"));
                    }
                }
            }
        } catch (Exception e) {
            Log.w(TAG, "Ex getting IP value " + e.getMessage());
        }

        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(Constants.NU_IP, address);
        edit.commit();
    }

    private void setImei(Context context) {
        String myIMEI = "0000000000000000";
        TelephonyManager mTelephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        int permissionCheck = ContextCompat.checkSelfPermission(
                context, Manifest.permission.READ_PHONE_STATE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            Log.i("Mensaje", "No se tiene permiso.");
            //ActivityCompat.requestPermissions(, new String[]{Manifest.permission.READ_PHONE_STATE }, 225);
        } else {
            if (mTelephony.getDeviceId() != null) {
                myIMEI = mTelephony.getDeviceId();
            }
        }

        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putLong(Constants.NU_IMEI, Long.valueOf(myIMEI));
        edit.commit();
    }

    private void setLocation(Context context) {
        Long time = sharedPreferences.getLong(Constants.KEY_INTERVAL_LOCATION_UPDATE, 1000L);
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(time);
        mLocationRequest.setFastestInterval(time / 2);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(10);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                locationNow = locationResult.getLastLocation();
            }
        };
    }

    private void requestLocationUpdates() {
        try {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
        } catch (SecurityException unlikely) {
            Log.e(TAG, "Lost location permission. Could not request updates. " + unlikely);
        }
    }
}
