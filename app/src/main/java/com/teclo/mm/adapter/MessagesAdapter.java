package com.teclo.mm.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.teclo.mm.R;
import com.teclo.mm.model.MessageMod;
import com.teclo.mm.util.RecyclerViewClickListener;
import com.teclo.mm.util.Utils;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * Created by brad on 2017/02/12.
 */

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.MessagesViewHolder> {

    private RecyclerViewClickListener mListener;
    List<MessageMod> listMessages;

    public MessagesAdapter(List<MessageMod> messages, RecyclerViewClickListener listener){
        listMessages = messages;
        mListener = listener;
    }

    class MessagesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView textViewFrom;
        TextView textViewMessage;
        TextView textViewTime;

        ImageView imageViewMessageDisplay;
        private RecyclerViewClickListener mListener;

        public MessagesViewHolder(View itemView, RecyclerViewClickListener listener) {
            super(itemView);
            textViewFrom = itemView.findViewById(R.id.text_view_from);
            textViewMessage = itemView.findViewById(R.id.text_view_message);
            textViewTime = itemView.findViewById(R.id.text_view_time);
            imageViewMessageDisplay = itemView.findViewById(R.id.image_view_message_display);

            mListener = listener;
            itemView.setOnClickListener(this);
        }

        public void setFrom(String username) {
            textViewFrom.setText(username);
        }

        public void setText(String message){
            textViewMessage.setText(message);
        }

        public void setTime(String dateItem){
            Date now = Utils.toDate("dd/MM/yyyy HH:mm:ss", "");
            Date item = Utils.toDate("dd/MM/yyyy HH:mm:ss", dateItem);

            long diffInMillies = Math.abs(now.getTime() - item.getTime());
            long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

            if(diff > 0){
                textViewTime.setText(Utils.getDate("dd/MM/yyyy", item));
            }else{
                textViewTime.setText(Utils.getHourMinutes(item));
            }
        }

        @Override
        public void onClick(View v) {
            MessageMod item = listMessages.get(getAdapterPosition());
            mListener.onClick(v, item);
        }
    }

    public void updateItems(List<MessageMod> messages){
        listMessages.clear();
        listMessages.addAll(messages);
        notifyDataSetChanged();
    }

    @Override
    public MessagesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_contact, parent, false);
        return new MessagesViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(MessagesViewHolder holder, int position) {
        holder.setFrom(listMessages.get(position).getFrom());
        holder.setText(listMessages.get(position).getText());
        holder.setTime(listMessages.get(position).getDate());
    }

    @Override
    public int getItemCount() {
        return listMessages.size();
    }
}
