package com.teclo.mm.commons;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

/**
 * Created by Jesus Gutierrez on 08/09/2017.
 */

public class Constants {

    /**SERVER API*/
    //public static final String BASE_URL = "http://ext.teclo.mx/teclomm_localizacion_v100r2/";
    public static final String BASE_URL = "http://172.18.44.101:9080/teclocdmxmonimovi-wsw_v100r2/";

    /** IP develop */
    //public static final String BASE_URL = "http://192.168.15.20:9088/teclocdmxmonimovi-wsw_v100r2/";
    //public static final String BASE_URL = "http://172.18.44.60:9080/teclocdmxmonimovi_v100r2/";
    //public static final String BASE_URL = "http://172.18.44.60:9080/teclocdmxmonimovi-wsw_v100r2/";

    /**TEST SERVER API*/
    public static final String API_MOVIL = "http://172.18.44.120:9080/teclocdmxgps_wsw_v100r1/movil/";
    public static final String API_BASE = "http://172.18.44.120:9080/teclocdmxgps_wsw_v100r1/";

    /**GOOGLE*/
    public static final String TEST_CONNECTION = "http://www.google.com/";
    public static final String GOOGLE_DIRECTIONS = "https://maps.googleapis.com/maps/api/";
    public static final LatLngBounds BOUNDS_MEXICO = new LatLngBounds(new LatLng(14.3895, -118.6523001), new LatLng(32.7186534, -86.5887));
    /**PREFERENCES*/
    public static final String PREFERENCE_FILE = "storage_routes";
    public static final String KEY_REQUESTING_LOCATION_UPDATES = "requesting_location_updates";
    public static final String KEY_INTERVAL_LOCATION_UPDATE = "key_interval_location_update";
    public static final String KEY_ROUTE= "key_route";
    public static final String KEY_USER_LOGGED = "key_user";
    public static final String KEY_ROUTE_GENERATED = "key_route_generated";
    public static final String FRAGMENT_LAUNCH_SERVICE = "fragment_launch_service";
    public static final String NU_IMEI = "nu_imei";
    public static final String NU_IP = "nu_ip";
    public static final String ID_EVENTO = "id_evento";
    /**PERMISSIONS*/
    public static final int PETICION_PERMISO_LOCALIZACION = 101;

    public final static long CONNECTION_TIMEOUT = 60;
    public final static String LOGIN_TOKEN = "login_token";
    public final static String DEVICE_SERIAL_NUMBER = "device_serial_number";
    public final static String DEVICE_ID = "device_id";

    public static final String NOTIFICATION_MESSAGE_KEY = "notification_message_key";

    /***/
    public static final String MOBILE_DATA_ACTION = "mobile";

    public static final String ACTION = "click_action";
    public static final String CMD_ACTION = "action";
    public static final String CMD_BODY = "body";
    public static final String SOUND_ON_ACTION = "sound_on";
    public static final String SOUND_OFF_ACTION = "sound_off";
    public static final String WIFI_ON_ACTION = "wifi_on";
    public static final String WIFI_OFF_ACTION = "wifi_off";
    public static final String GPS_ACTION = "gps_actual";
}
