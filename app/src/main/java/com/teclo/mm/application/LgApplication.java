package com.teclo.mm.application;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;

import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Jesus Gutierrez on 18/10/2017.
 */

public class LgApplication extends Application {

    public final static String TAG = LgApplication.class.getSimpleName();
    private static LgApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        /**Instanciando la librería Realm  */
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder().deleteRealmIfMigrationNeeded().build();
        Realm.setDefaultConfiguration(config);

        setCustomLocale();
    }

    public static Context getContext(){
        return instance;
    }

    private void setCustomLocale(){
        Locale locale = new Locale("es","MX");
        //Locale locale = Locale.getDefault();
        Locale.setDefault(locale);
        Configuration config = getBaseContext().getResources().getConfiguration();
        config.setLocale(locale);
        createConfigurationContext(config);
    }
}
