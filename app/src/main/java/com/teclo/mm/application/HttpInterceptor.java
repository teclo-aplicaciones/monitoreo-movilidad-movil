package com.teclo.mm.application;

import android.content.SharedPreferences;
import android.util.Log;

import com.teclo.mm.commons.Constants;
import com.teclo.mm.util.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okio.Buffer;

public class HttpInterceptor implements Interceptor {

    public final static String TAG = HttpInterceptor.class.getSimpleName();

    //--- HTTP Response codes relative constants
    private static final int RESPONSE_UNAUTHORIZED_401 = 401;
    private static final int RESPONSE_HTTP_RANK_2XX = 2;
    private static final int RESPONSE_HTTP_CLIENT_ERROR = 4;
    private static final int RESPONSE_HTTP_SERVER_ERROR = 5;

    SharedPreferences settings = null;
    public HttpInterceptor() {
        if(settings == null){}
            settings = PreferenceManager.sharedInstance(LgApplication.getContext()).getPackagePreferences();
    }

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request request = chain.request();                  //<<< Original Request

        //Build new request-----------------------------
        Request.Builder builder = request.newBuilder();
        builder.header("Accept", "application/json");       //if necessary...

        String token = settings.getString(Constants.LOGIN_TOKEN, null);//Save token of this request for future
        setAuthHeader(builder, token);                      //Add Current Authentication Token..

        request = builder.build();                          //Overwrite the original request

        Log.d(TAG,
                ">>> Sending Request >>>\n"
                        +"To: "+request.url()+"\n"
                        +"Headers:"+request.headers()+"\n"
                        +"Body: "+bodyToString(request));   //Shows the magic...

        //------------------------------------------------------------------------------------------
        Response response = chain.proceed(request);         // Sends the request (Original w/ Auth.)
        //------------------------------------------------------------------------------------------

        Log.d(TAG,
                "<<< Receiving Request response <<<\n"
                        +"To: "+response.request().url()+"\n"
                        +"Headers: "+response.headers()+"\n"
                        +"Code: "+response.code()+"\n"
                        +"Body: "+bodyToString(response.request()));  //Shows the magic...



        //------------------- 401 --- 401 --- UNAUTHORIZED --- 401 --- 401 -------------------------

        if (response.code() == RESPONSE_UNAUTHORIZED_401) { //If unauthorized (Token expired)...
            Log.w(TAG,"Request responses code: "+response.code());

            synchronized (this) {                           // Gets all 401 in sync blocks,
                // to avoid multiply token updates...

                String currentToken = settings.getString(Constants.LOGIN_TOKEN, null); //Get currently stored token (...)

                //Compares current token with token that was stored before,
                // if it was not updated - do update..

                if(currentToken != null && currentToken.equals(token)) {

                    // --- REFRESHING TOKEN --- --- REFRESHING TOKEN --- --- REFRESHING TOKEN ------

                    int code = refreshToken() / 100;                    //Refactor resp. cod ranking

                    if(code != RESPONSE_HTTP_RANK_2XX) {                // If refresh token failed

                        if(code == RESPONSE_HTTP_CLIENT_ERROR           // If failed by error 4xx...
                                ||
                                code == RESPONSE_HTTP_SERVER_ERROR ){   // If failed by error 5xx...

                            logout();                                   // ToDo GoTo login screen
                            return response;                            // Todo Shows auth error to user
                        }
                    }   // <<--------------------------------------------New Auth. Token acquired --
                }   // <<-----------------------------------New Auth. Token acquired double check --


                // --- --- RETRYING ORIGINAL REQUEST --- --- RETRYING ORIGINAL REQUEST --- --------|

                String tokenUpdate = settings.getString(Constants.LOGIN_TOKEN, null);
                if(tokenUpdate != null) {                  // Checks new Auth. Token
                    setAuthHeader(builder, tokenUpdate);   // Add Current Auth. Token
                    request = builder.build();                          // O/w the original request

                    Log.d(TAG,
                            ">>> Retrying original Request >>>\n"
                                    +"To: "+request.url()+"\n"
                                    +"Headers:"+request.headers()+"\n"
                                    +"Body: "+bodyToString(request));  //Shows the magic...


                    //-----------------------------------------------------------------------------|
                    Response responseRetry = chain.proceed(request);// Sends request (w/ New Auth.)
                    //-----------------------------------------------------------------------------|


                    Log.d(TAG,
                            "<<< Receiving Retried Request response <<<\n"
                                    +"To: "+responseRetry.request().url()+"\n"
                                    +"Headers: "+responseRetry.headers()+"\n"
                                    +"Code: "+responseRetry.code()+"\n"
                                    +"Body: "+bodyToString(response.request()));  //Shows the magic.

                    return responseRetry;
                }
            }
        }else {
            //------------------- 200 --- 200 --- AUTHORIZED --- 200 --- 200 -----------------------
            Log.w(TAG,"Request responses code: "+response.code());
        }

        return response;

    }

    private void setAuthHeader(Request.Builder builder, String token) {
        if (token != null) //Add Auth token to each request if authorized
            builder.header("X-Auth-Token", token); //String.format("Bearer %s", token)
    }

    // Refresh/renew Synchronously Authentication Token & refresh token----------------------------|
    private int refreshToken() {
        Log.w(TAG,"Refreshing tokens... ;o");

        // Builds a client...
        OkHttpClient client = new OkHttpClient.Builder().build();

        // Builds a Request Body...for renewing token...
        //MediaType jsonType = MediaType.parse("application/json; charset=utf-8");
        //JsonObject json = new JsonObject();
        //---
        //json.addProperty(BODY_PARAM_KEY_GRANT_TYPE, BODY_PARAM_VALUE_GRANT_TYPE);
        //json.addProperty(BODY_PARAM_KEY_REFRESH_TOKEN,Session.getRefreshAccessToken());
        //---
        //RequestBody body = RequestBody.create(jsonType,json.toString());
        // Builds a request with request body...
        Request request = new Request.Builder()
                .header("X-Auth-Token",settings.getString(Constants.LOGIN_TOKEN, ""))
                .url(Constants.BASE_URL+"login/refresh")
                //.post(body)                     //<<<--------------Adds body (Token renew by the way)
                .build();


        Response response = null;
        int code = 0;

        Log.d(TAG,
                ">>> Sending Refresh Token Request >>>\n"
                        +"To: "+request.url()+"\n"
                        +"Headers:"+request.headers()+"\n"
                        +"Body: "+bodyToString(request));  //Shows the magic...
        try {
            //--------------------------------------------------------------------------------------
            response = client.newCall(request).execute();       //Sends Refresh token request
            //--------------------------------------------------------------------------------------

            Log.d(TAG,
                    "<<< Receiving Refresh Token Request Response <<<\n"
                            +"To: "+response.request().url()+"\n"
                            +"Headers:"+response.headers()+"\n"
                            +"Code: "+response.code()+"\n"
                            +"Body: "+bodyToString(response.request()));  //Shows the magic...

            if (response != null) {
                code = response.code();
                Log.i(TAG,"Token Refresh responses code: "+code);

                switch (code){
                    case 200:
                        // READS NEW TOKENS AND SAVES THEM -----------------------------------------
                        try {
                            //Log.i(TAG,"Decoding tokens start");
                            JSONObject jsonBody = null;
                            jsonBody = new JSONObject(response.body().string());
                            String newAuthtoken = jsonBody.getString("token");
                            //String tokenRefresh = jsonBody.getString("refresh_token");

                            Log.i(TAG,"New Access Token = "+newAuthtoken);
                           // Log.i(TAG,"New Refresh Token = "+tokenRefresh);

                            SharedPreferences.Editor editor = settings.edit();
                            editor.putString(Constants.LOGIN_TOKEN, newAuthtoken);
                            editor.commit();
                            //Session.setRefreshAccessToken(tokenRefresh);
                            //Log.i(TAG,"Decoding tokens finish.");

                        } catch (JSONException e) {
                            Log.w(TAG,"Responses code "+ code
                                    +" but error getting response body.\n"
                                    + e.getMessage());
                        }

                        break;

                    default:

                        // READS ERROR -------------------------------------------------------------

                        try {
                            //Log.i(TAG,"Decoding error start");
                            JSONObject jsonBodyE = null;
                            jsonBodyE = new JSONObject(response.body().string());
                            String error = jsonBodyE.getString("message");
                            //String errorDescription = jsonBodyE.getString("error_description");

                            //Log.i(TAG,"Decoding tokens finish.");

                        } catch (JSONException e) {
                            Log.w(TAG,"Responses code "+ code
                                    +" but error getting response body.\n"
                                    + e.getMessage());
                            e.printStackTrace();
                        }
                        break;
                }


                response.body().close(); //ToDo check this line
            }

        } catch (IOException e) {
            Log.w(TAG,"Error while Sending Refresh Token Request\n"+e.getMessage());
            e.printStackTrace();
        }
        return code;
    }

    private int logout() {
        Log.d(TAG, "go to logout");
        return 0;
    }

    @Deprecated
    private static String bodyToString(final Request request){

        try {
            final Request copy = request.newBuilder().build();
            final Buffer buffer = new Buffer();
            if(copy.body() != null) {
                copy.body().writeTo(buffer);
                return buffer.readUtf8();
            }
        } catch (final IOException e) {
            Log.w(TAG,"Error while trying to get body to string.");
            return "Null";
        }

        return "Null";
    }
}